/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.tests;

import junit.textui.TestRunner;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory;
import pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Operation Order</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class Operation_OrderTest extends NodeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(Operation_OrderTest.class);
	}

	/**
	 * Constructs a new Operation Order test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation_OrderTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Operation Order test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Operation_Order getFixture() {
		return (Operation_Order)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(KAOS4ServiceFactory.eINSTANCE.createOperation_Order());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //Operation_OrderTest
