/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.tests;

import junit.textui.TestRunner;

import pt.unl.fct.KAOS4Service.KAOS4Service.Expectation;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Expectation</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpectationTest extends LeafGoalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ExpectationTest.class);
	}

	/**
	 * Constructs a new Expectation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpectationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Expectation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Expectation getFixture() {
		return (Expectation)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(KAOS4ServiceFactory.eINSTANCE.createExpectation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ExpectationTest
