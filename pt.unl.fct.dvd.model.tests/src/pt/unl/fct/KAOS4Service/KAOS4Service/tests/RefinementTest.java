/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.tests;

import junit.textui.TestRunner;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory;
import pt.unl.fct.KAOS4Service.KAOS4Service.Refinement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefinementTest extends RelationTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RefinementTest.class);
	}

	/**
	 * Constructs a new Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Refinement getFixture() {
		return (Refinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(KAOS4ServiceFactory.eINSTANCE.createRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RefinementTest
