/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.tests;

import junit.textui.TestRunner;

import pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Environment Agent</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EnvironmentAgentTest extends AgentTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EnvironmentAgentTest.class);
	}

	/**
	 * Constructs a new Environment Agent test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgentTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Environment Agent test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EnvironmentAgent getFixture() {
		return (EnvironmentAgent)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(KAOS4ServiceFactory.eINSTANCE.createEnvironmentAgent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EnvironmentAgentTest
