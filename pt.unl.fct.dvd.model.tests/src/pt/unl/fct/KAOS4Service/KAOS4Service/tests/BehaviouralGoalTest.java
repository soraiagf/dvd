/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.tests;

import junit.textui.TestRunner;

import pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Behavioural Goal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BehaviouralGoalTest extends GoalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BehaviouralGoalTest.class);
	}

	/**
	 * Constructs a new Behavioural Goal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BehaviouralGoalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Behavioural Goal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BehaviouralGoal getFixture() {
		return (BehaviouralGoal)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(KAOS4ServiceFactory.eINSTANCE.createBehaviouralGoal());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BehaviouralGoalTest
