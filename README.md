A ferramenta DVDTool permite fazer o alinhamento entre a área de negócios e a área de tecnologia de informação, através da construção de modelos de valor e a geração por transformação de modelos de requisitos e arquitetura de serviços.

##Qual o propósito deste repositório?
Este é o repositório para a DVDTool desenvolvida com Sirius.
De momento, este projecto é gerido por uma equipa de Engenharia de Software da FCT/UNL, que é responsável por todas as atualizações. 

 Membros da equipa: Soraia Freitas (FCT/UNL, Estudante Mestrado), Eric Souza (FCT/UNL, Estudante Doutoramento) e Ana Moreira (FCT/UNL, Supervisora)
   
##Como configurar a ferramenta?

Para configurar a ferramenta DVDTool, este repositório (master branch) deve ser clonado usando um cliente git.

    Estrutura do Projeto: Existem 7 pastas principais no repositório a ser clonado
        pt.unl.fct.dvd.design: contém a descrição dos editores Sirius\dvd.odesign
        pt.unl.fct.dvd.model: contém os modelos ecore
        pt.unl.fct.dvd.model.edit: ficheiros internos relativos à edição de modelos
        pt.unl.fct.dvd.model.editor: ficheiros internos relativos aos editores
        pt.unl.fct.dvd.model.tests: ficheiros relativos aos modelos EMF
        pt.unl.fct.dvd.test: contém os editores da DVDTool criados
        pt.unl.fct.dvd.transformations: contém ficheiros relativos às transformações de modelos
---

##Ambiente de desenvolvimento
De seguida, apresentamos o ambiente de desenvolvimento da ferramenta DVDTool.

    Eclipse
        Eclipse Modeling Project - Versão: Oxygen Release (4.7.0) Build id: (20170620-1800)
    EGit
        Versão (4.8.0)
    Sirius
        Versão (5.1.0.201709140800)
    Java
        Versão (1.8.0_60)

##Como executar os editores?
        No projecto, abrir a pasta pt.unl.fct.dmt.test. Esta pasta, possui vários ficheiros main, cuja 
        terminação diz respeito ao modelo que representa. 
        Dentro de cada um destes ficheiros existe o editor do respetivo modelo.
    
##Como entrar em contacto?

Para qualquer dúvida ou questão sobre a ferramenta, não hesite em contactar-nos.

Soraia Freitas (s.freitas@campus.fct.unl.pt)
---