/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Value Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getInValueObject()
 * @model
 * @generated
 */
public interface InValueObject extends Port {
} // InValueObject
