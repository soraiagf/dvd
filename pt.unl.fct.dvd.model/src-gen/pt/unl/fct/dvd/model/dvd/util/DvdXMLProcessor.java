/**
 */
package pt.unl.fct.dvd.model.dvd.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import pt.unl.fct.dvd.model.dvd.DvdPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DvdXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DvdXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		DvdPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the DvdResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new DvdResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new DvdResourceFactoryImpl());
		}
		return registrations;
	}

} //DvdXMLProcessor
