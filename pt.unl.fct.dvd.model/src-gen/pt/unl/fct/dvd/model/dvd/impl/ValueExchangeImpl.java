/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.EnvironmentActor;
import pt.unl.fct.dvd.model.dvd.InValueObject;
import pt.unl.fct.dvd.model.dvd.OutValueObject;
import pt.unl.fct.dvd.model.dvd.ValueExchange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Exchange</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl#getId <em>Id</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl#getHasEnvironmentActor <em>Has Environment Actor</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl#getInValueObject <em>In Value Object</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl#getOutValueObject <em>Out Value Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueExchangeImpl extends NodesImpl implements ValueExchange {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasEnvironmentActor() <em>Has Environment Actor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasEnvironmentActor()
	 * @generated
	 * @ordered
	 */
	protected EnvironmentActor hasEnvironmentActor;

	/**
	 * The cached value of the '{@link #getInValueObject() <em>In Value Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInValueObject()
	 * @generated
	 * @ordered
	 */
	protected InValueObject inValueObject;

	/**
	 * The cached value of the '{@link #getOutValueObject() <em>Out Value Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutValueObject()
	 * @generated
	 * @ordered
	 */
	protected OutValueObject outValueObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueExchangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.VALUE_EXCHANGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_EXCHANGE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_EXCHANGE__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentActor getHasEnvironmentActor() {
		if (hasEnvironmentActor != null && hasEnvironmentActor.eIsProxy()) {
			InternalEObject oldHasEnvironmentActor = (InternalEObject) hasEnvironmentActor;
			hasEnvironmentActor = (EnvironmentActor) eResolveProxy(oldHasEnvironmentActor);
			if (hasEnvironmentActor != oldHasEnvironmentActor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR, oldHasEnvironmentActor,
							hasEnvironmentActor));
			}
		}
		return hasEnvironmentActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentActor basicGetHasEnvironmentActor() {
		return hasEnvironmentActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasEnvironmentActor(EnvironmentActor newHasEnvironmentActor,
			NotificationChain msgs) {
		EnvironmentActor oldHasEnvironmentActor = hasEnvironmentActor;
		hasEnvironmentActor = newHasEnvironmentActor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR, oldHasEnvironmentActor, newHasEnvironmentActor);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasEnvironmentActor(EnvironmentActor newHasEnvironmentActor) {
		if (newHasEnvironmentActor != hasEnvironmentActor) {
			NotificationChain msgs = null;
			if (hasEnvironmentActor != null)
				msgs = ((InternalEObject) hasEnvironmentActor).eInverseRemove(this,
						DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE, EnvironmentActor.class, msgs);
			if (newHasEnvironmentActor != null)
				msgs = ((InternalEObject) newHasEnvironmentActor).eInverseAdd(this,
						DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE, EnvironmentActor.class, msgs);
			msgs = basicSetHasEnvironmentActor(newHasEnvironmentActor, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR,
					newHasEnvironmentActor, newHasEnvironmentActor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InValueObject getInValueObject() {
		if (inValueObject != null && inValueObject.eIsProxy()) {
			InternalEObject oldInValueObject = (InternalEObject) inValueObject;
			inValueObject = (InValueObject) eResolveProxy(oldInValueObject);
			if (inValueObject != oldInValueObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT, oldInValueObject, inValueObject));
			}
		}
		return inValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InValueObject basicGetInValueObject() {
		return inValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInValueObject(InValueObject newInValueObject) {
		InValueObject oldInValueObject = inValueObject;
		inValueObject = newInValueObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT,
					oldInValueObject, inValueObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutValueObject getOutValueObject() {
		if (outValueObject != null && outValueObject.eIsProxy()) {
			InternalEObject oldOutValueObject = (InternalEObject) outValueObject;
			outValueObject = (OutValueObject) eResolveProxy(oldOutValueObject);
			if (outValueObject != oldOutValueObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT, oldOutValueObject, outValueObject));
			}
		}
		return outValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutValueObject basicGetOutValueObject() {
		return outValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutValueObject(OutValueObject newOutValueObject) {
		OutValueObject oldOutValueObject = outValueObject;
		outValueObject = newOutValueObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT,
					oldOutValueObject, outValueObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			if (hasEnvironmentActor != null)
				msgs = ((InternalEObject) hasEnvironmentActor).eInverseRemove(this,
						DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE, EnvironmentActor.class, msgs);
			return basicSetHasEnvironmentActor((EnvironmentActor) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			return basicSetHasEnvironmentActor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__ID:
			return getId();
		case DvdPackage.VALUE_EXCHANGE__DESCRIPTION:
			return getDescription();
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			if (resolve)
				return getHasEnvironmentActor();
			return basicGetHasEnvironmentActor();
		case DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT:
			if (resolve)
				return getInValueObject();
			return basicGetInValueObject();
		case DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT:
			if (resolve)
				return getOutValueObject();
			return basicGetOutValueObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__ID:
			setId((Integer) newValue);
			return;
		case DvdPackage.VALUE_EXCHANGE__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			setHasEnvironmentActor((EnvironmentActor) newValue);
			return;
		case DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT:
			setInValueObject((InValueObject) newValue);
			return;
		case DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT:
			setOutValueObject((OutValueObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__ID:
			setId(ID_EDEFAULT);
			return;
		case DvdPackage.VALUE_EXCHANGE__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			setHasEnvironmentActor((EnvironmentActor) null);
			return;
		case DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT:
			setInValueObject((InValueObject) null);
			return;
		case DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT:
			setOutValueObject((OutValueObject) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.VALUE_EXCHANGE__ID:
			return id != ID_EDEFAULT;
		case DvdPackage.VALUE_EXCHANGE__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR:
			return hasEnvironmentActor != null;
		case DvdPackage.VALUE_EXCHANGE__IN_VALUE_OBJECT:
			return inValueObject != null;
		case DvdPackage.VALUE_EXCHANGE__OUT_VALUE_OBJECT:
			return outValueObject != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //ValueExchangeImpl
