/**
 */
package pt.unl.fct.dvd.model.dvd.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see pt.unl.fct.dvd.model.dvd.util.DvdResourceFactoryImpl
 * @generated
 */
public class DvdResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public DvdResourceImpl(URI uri) {
		super(uri);
	}

} //DvdResourceImpl
