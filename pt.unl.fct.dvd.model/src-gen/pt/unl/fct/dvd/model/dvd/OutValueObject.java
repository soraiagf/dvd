/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Value Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getOutValueObject()
 * @model
 * @generated
 */
public interface OutValueObject extends Port {
} // OutValueObject
