/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import pt.unl.fct.dvd.model.dvd.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DvdFactoryImpl extends EFactoryImpl implements DvdFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DvdFactory init() {
		try {
			DvdFactory theDvdFactory = (DvdFactory) EPackage.Registry.INSTANCE.getEFactory(DvdPackage.eNS_URI);
			if (theDvdFactory != null) {
				return theDvdFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DvdFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DvdFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION:
			return createDynamicValueDescription();
		case DvdPackage.MAIN_ACTOR:
			return createMainActor();
		case DvdPackage.ENVIRONMENT_ACTOR:
			return createEnvironmentActor();
		case DvdPackage.WHO_START_RELATION:
			return createWhoStartRelation();
		case DvdPackage.SIMPLE_RELATION:
			return createSimpleRelation();
		case DvdPackage.VALUE_EXCHANGE:
			return createValueExchange();
		case DvdPackage.IN_VALUE_OBJECT:
			return createInValueObject();
		case DvdPackage.OUT_VALUE_OBJECT:
			return createOutValueObject();
		case DvdPackage.VALUE_LEVEL_AGREEMENT:
			return createValueLevelAgreement();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicValueDescription createDynamicValueDescription() {
		DynamicValueDescriptionImpl dynamicValueDescription = new DynamicValueDescriptionImpl();
		return dynamicValueDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MainActor createMainActor() {
		MainActorImpl mainActor = new MainActorImpl();
		return mainActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentActor createEnvironmentActor() {
		EnvironmentActorImpl environmentActor = new EnvironmentActorImpl();
		return environmentActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhoStartRelation createWhoStartRelation() {
		WhoStartRelationImpl whoStartRelation = new WhoStartRelationImpl();
		return whoStartRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleRelation createSimpleRelation() {
		SimpleRelationImpl simpleRelation = new SimpleRelationImpl();
		return simpleRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueExchange createValueExchange() {
		ValueExchangeImpl valueExchange = new ValueExchangeImpl();
		return valueExchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InValueObject createInValueObject() {
		InValueObjectImpl inValueObject = new InValueObjectImpl();
		return inValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutValueObject createOutValueObject() {
		OutValueObjectImpl outValueObject = new OutValueObjectImpl();
		return outValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueLevelAgreement createValueLevelAgreement() {
		ValueLevelAgreementImpl valueLevelAgreement = new ValueLevelAgreementImpl();
		return valueLevelAgreement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DvdPackage getDvdPackage() {
		return (DvdPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DvdPackage getPackage() {
		return DvdPackage.eINSTANCE;
	}

} //DvdFactoryImpl
