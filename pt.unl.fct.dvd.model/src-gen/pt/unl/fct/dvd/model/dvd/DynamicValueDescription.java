/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dynamic Value Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getNodes <em>Nodes</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getRelations <em>Relations</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getMainActor <em>Main Actor</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getDynamicValueDescription()
 * @model
 * @generated
 */
public interface DynamicValueDescription extends EObject {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link pt.unl.fct.dvd.model.dvd.Nodes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getDynamicValueDescription_Nodes()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Nodes> getNodes();

	/**
	 * Returns the value of the '<em><b>Relations</b></em>' containment reference list.
	 * The list contents are of type {@link pt.unl.fct.dvd.model.dvd.Relation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations</em>' containment reference list.
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getDynamicValueDescription_Relations()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Relation> getRelations();

	/**
	 * Returns the value of the '<em><b>Main Actor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Actor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Actor</em>' containment reference.
	 * @see #setMainActor(MainActor)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getDynamicValueDescription_MainActor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MainActor getMainActor();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getMainActor <em>Main Actor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Actor</em>' containment reference.
	 * @see #getMainActor()
	 * @generated
	 */
	void setMainActor(MainActor value);

} // DynamicValueDescription
