/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nodes</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getNodes()
 * @model abstract="true"
 * @generated
 */
public interface Nodes extends EObject {
} // Nodes
