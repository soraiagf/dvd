/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.ValueExchange;
import pt.unl.fct.dvd.model.dvd.WhoStartRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Who Start Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl#getHasVEForward <em>Has VE Forward</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl#getHasVEBackward <em>Has VE Backward</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WhoStartRelationImpl extends RelationImpl implements WhoStartRelation {
	/**
	 * The cached value of the '{@link #getHasVEForward() <em>Has VE Forward</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasVEForward()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueExchange> hasVEForward;

	/**
	 * The cached value of the '{@link #getHasVEBackward() <em>Has VE Backward</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasVEBackward()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueExchange> hasVEBackward;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhoStartRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.WHO_START_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueExchange> getHasVEForward() {
		if (hasVEForward == null) {
			hasVEForward = new EObjectResolvingEList<ValueExchange>(ValueExchange.class, this,
					DvdPackage.WHO_START_RELATION__HAS_VE_FORWARD);
		}
		return hasVEForward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueExchange> getHasVEBackward() {
		if (hasVEBackward == null) {
			hasVEBackward = new EObjectResolvingEList<ValueExchange>(ValueExchange.class, this,
					DvdPackage.WHO_START_RELATION__HAS_VE_BACKWARD);
		}
		return hasVEBackward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.WHO_START_RELATION__HAS_VE_FORWARD:
			return getHasVEForward();
		case DvdPackage.WHO_START_RELATION__HAS_VE_BACKWARD:
			return getHasVEBackward();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.WHO_START_RELATION__HAS_VE_FORWARD:
			getHasVEForward().clear();
			getHasVEForward().addAll((Collection<? extends ValueExchange>) newValue);
			return;
		case DvdPackage.WHO_START_RELATION__HAS_VE_BACKWARD:
			getHasVEBackward().clear();
			getHasVEBackward().addAll((Collection<? extends ValueExchange>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.WHO_START_RELATION__HAS_VE_FORWARD:
			getHasVEForward().clear();
			return;
		case DvdPackage.WHO_START_RELATION__HAS_VE_BACKWARD:
			getHasVEBackward().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.WHO_START_RELATION__HAS_VE_FORWARD:
			return hasVEForward != null && !hasVEForward.isEmpty();
		case DvdPackage.WHO_START_RELATION__HAS_VE_BACKWARD:
			return hasVEBackward != null && !hasVEBackward.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //WhoStartRelationImpl
