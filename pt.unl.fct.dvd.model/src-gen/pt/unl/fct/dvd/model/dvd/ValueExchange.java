/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Exchange</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getId <em>Id</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor <em>Has Environment Actor</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getInValueObject <em>In Value Object</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getOutValueObject <em>Out Value Object</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange()
 * @model
 * @generated
 */
public interface ValueExchange extends Nodes {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Has Environment Actor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getHasValueExchange <em>Has Value Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Environment Actor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Environment Actor</em>' reference.
	 * @see #setHasEnvironmentActor(EnvironmentActor)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange_HasEnvironmentActor()
	 * @see pt.unl.fct.dvd.model.dvd.EnvironmentActor#getHasValueExchange
	 * @model opposite="hasValueExchange" required="true"
	 * @generated
	 */
	EnvironmentActor getHasEnvironmentActor();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor <em>Has Environment Actor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Environment Actor</em>' reference.
	 * @see #getHasEnvironmentActor()
	 * @generated
	 */
	void setHasEnvironmentActor(EnvironmentActor value);

	/**
	 * Returns the value of the '<em><b>In Value Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Value Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Value Object</em>' reference.
	 * @see #setInValueObject(InValueObject)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange_InValueObject()
	 * @model required="true"
	 * @generated
	 */
	InValueObject getInValueObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getInValueObject <em>In Value Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Value Object</em>' reference.
	 * @see #getInValueObject()
	 * @generated
	 */
	void setInValueObject(InValueObject value);

	/**
	 * Returns the value of the '<em><b>Out Value Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Value Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Value Object</em>' reference.
	 * @see #setOutValueObject(OutValueObject)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueExchange_OutValueObject()
	 * @model required="true"
	 * @generated
	 */
	OutValueObject getOutValueObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getOutValueObject <em>Out Value Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Value Object</em>' reference.
	 * @see #getOutValueObject()
	 * @generated
	 */
	void setOutValueObject(OutValueObject value);

} // ValueExchange
