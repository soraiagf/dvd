/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.EnvironmentActor;
import pt.unl.fct.dvd.model.dvd.ValueExchange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl#getHasValueExchange <em>Has Value Exchange</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl#getIdEnvironmentActor <em>Id Environment Actor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentActorImpl extends ActorImpl implements EnvironmentActor {
	/**
	 * The cached value of the '{@link #getHasValueExchange() <em>Has Value Exchange</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasValueExchange()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueExchange> hasValueExchange;

	/**
	 * The default value of the '{@link #getIdEnvironmentActor() <em>Id Environment Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdEnvironmentActor()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_ENVIRONMENT_ACTOR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIdEnvironmentActor() <em>Id Environment Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdEnvironmentActor()
	 * @generated
	 * @ordered
	 */
	protected int idEnvironmentActor = ID_ENVIRONMENT_ACTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentActorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.ENVIRONMENT_ACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueExchange> getHasValueExchange() {
		if (hasValueExchange == null) {
			hasValueExchange = new EObjectWithInverseResolvingEList<ValueExchange>(ValueExchange.class, this,
					DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE, DvdPackage.VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR);
		}
		return hasValueExchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIdEnvironmentActor() {
		return idEnvironmentActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdEnvironmentActor(int newIdEnvironmentActor) {
		int oldIdEnvironmentActor = idEnvironmentActor;
		idEnvironmentActor = newIdEnvironmentActor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR,
					oldIdEnvironmentActor, idEnvironmentActor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getHasValueExchange()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			return ((InternalEList<?>) getHasValueExchange()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			return getHasValueExchange();
		case DvdPackage.ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR:
			return getIdEnvironmentActor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			getHasValueExchange().clear();
			getHasValueExchange().addAll((Collection<? extends ValueExchange>) newValue);
			return;
		case DvdPackage.ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR:
			setIdEnvironmentActor((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			getHasValueExchange().clear();
			return;
		case DvdPackage.ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR:
			setIdEnvironmentActor(ID_ENVIRONMENT_ACTOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE:
			return hasValueExchange != null && !hasValueExchange.isEmpty();
		case DvdPackage.ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR:
			return idEnvironmentActor != ID_ENVIRONMENT_ACTOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (idEnvironmentActor: ");
		result.append(idEnvironmentActor);
		result.append(')');
		return result.toString();
	}

} //EnvironmentActorImpl
