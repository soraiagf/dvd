/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.Port#getObject <em>Object</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.Port#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.Port#getIdObject <em>Id Object</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getPort()
 * @model abstract="true"
 * @generated
 */
public interface Port extends Nodes {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' attribute.
	 * @see #setObject(String)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getPort_Object()
	 * @model
	 * @generated
	 */
	String getObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.Port#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' attribute.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getPort_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.Port#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Id Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Object</em>' attribute.
	 * @see #setIdObject(int)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getPort_IdObject()
	 * @model required="true"
	 * @generated
	 */
	int getIdObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.Port#getIdObject <em>Id Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Object</em>' attribute.
	 * @see #getIdObject()
	 * @generated
	 */
	void setIdObject(int value);

} // Port
