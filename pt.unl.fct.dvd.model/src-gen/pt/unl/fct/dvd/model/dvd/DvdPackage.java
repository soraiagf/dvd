/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see pt.unl.fct.dvd.model.dvd.DvdFactory
 * @model kind="package"
 * @generated
 */
public interface DvdPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dvd";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "pt.unl.fct.dvd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "pt.unl.fct.dvd";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DvdPackage eINSTANCE = pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl.init();

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl <em>Dynamic Value Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getDynamicValueDescription()
	 * @generated
	 */
	int DYNAMIC_VALUE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_VALUE_DESCRIPTION__NODES = 0;

	/**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_VALUE_DESCRIPTION__RELATIONS = 1;

	/**
	 * The feature id for the '<em><b>Main Actor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR = 2;

	/**
	 * The number of structural features of the '<em>Dynamic Value Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_VALUE_DESCRIPTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Dynamic Value Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_VALUE_DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.NodesImpl <em>Nodes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.NodesImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getNodes()
	 * @generated
	 */
	int NODES = 1;

	/**
	 * The number of structural features of the '<em>Nodes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODES_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Nodes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.ActorImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAME = NODES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = NODES_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_OPERATION_COUNT = NODES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.MainActorImpl <em>Main Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.MainActorImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getMainActor()
	 * @generated
	 */
	int MAIN_ACTOR = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_ACTOR__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Id Main Actor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_ACTOR__ID_MAIN_ACTOR = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Main Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_ACTOR_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Main Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_ACTOR_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl <em>Environment Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getEnvironmentActor()
	 * @generated
	 */
	int ENVIRONMENT_ACTOR = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_ACTOR__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Has Value Exchange</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id Environment Actor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR = ACTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Environment Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_ACTOR_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Environment Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_ACTOR_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.RelationImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 5;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__TO = 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__FROM = 1;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl <em>Who Start Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getWhoStartRelation()
	 * @generated
	 */
	int WHO_START_RELATION = 6;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION__TO = RELATION__TO;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION__FROM = RELATION__FROM;

	/**
	 * The feature id for the '<em><b>Has VE Forward</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION__HAS_VE_FORWARD = RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has VE Backward</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION__HAS_VE_BACKWARD = RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Who Start Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION_FEATURE_COUNT = RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Who Start Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHO_START_RELATION_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.SimpleRelationImpl <em>Simple Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.SimpleRelationImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getSimpleRelation()
	 * @generated
	 */
	int SIMPLE_RELATION = 7;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_RELATION__TO = RELATION__TO;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_RELATION__FROM = RELATION__FROM;

	/**
	 * The number of structural features of the '<em>Simple Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_RELATION_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Simple Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_RELATION_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl <em>Value Exchange</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getValueExchange()
	 * @generated
	 */
	int VALUE_EXCHANGE = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE__ID = NODES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE__DESCRIPTION = NODES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Environment Actor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR = NODES_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>In Value Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE__IN_VALUE_OBJECT = NODES_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out Value Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE__OUT_VALUE_OBJECT = NODES_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Value Exchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE_FEATURE_COUNT = NODES_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Value Exchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXCHANGE_OPERATION_COUNT = NODES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.PortImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 9;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__OBJECT = NODES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DESCRIPTION = NODES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ID_OBJECT = NODES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = NODES_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_OPERATION_COUNT = NODES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.InValueObjectImpl <em>In Value Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.InValueObjectImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getInValueObject()
	 * @generated
	 */
	int IN_VALUE_OBJECT = 10;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_VALUE_OBJECT__OBJECT = PORT__OBJECT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_VALUE_OBJECT__DESCRIPTION = PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_VALUE_OBJECT__ID_OBJECT = PORT__ID_OBJECT;

	/**
	 * The number of structural features of the '<em>In Value Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_VALUE_OBJECT_FEATURE_COUNT = PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>In Value Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_VALUE_OBJECT_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.OutValueObjectImpl <em>Out Value Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.OutValueObjectImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getOutValueObject()
	 * @generated
	 */
	int OUT_VALUE_OBJECT = 11;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_VALUE_OBJECT__OBJECT = PORT__OBJECT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_VALUE_OBJECT__DESCRIPTION = PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_VALUE_OBJECT__ID_OBJECT = PORT__ID_OBJECT;

	/**
	 * The number of structural features of the '<em>Out Value Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_VALUE_OBJECT_FEATURE_COUNT = PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Out Value Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_VALUE_OBJECT_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl <em>Value Level Agreement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl
	 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getValueLevelAgreement()
	 * @generated
	 */
	int VALUE_LEVEL_AGREEMENT = 12;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LEVEL_AGREEMENT__DESCRIPTION = NODES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id VLA</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LEVEL_AGREEMENT__ID_VLA = NODES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Value Level Agreement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LEVEL_AGREEMENT_FEATURE_COUNT = NODES_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Value Level Agreement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LEVEL_AGREEMENT_OPERATION_COUNT = NODES_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription <em>Dynamic Value Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic Value Description</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.DynamicValueDescription
	 * @generated
	 */
	EClass getDynamicValueDescription();

	/**
	 * Returns the meta object for the containment reference list '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getNodes()
	 * @see #getDynamicValueDescription()
	 * @generated
	 */
	EReference getDynamicValueDescription_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getRelations <em>Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getRelations()
	 * @see #getDynamicValueDescription()
	 * @generated
	 */
	EReference getDynamicValueDescription_Relations();

	/**
	 * Returns the meta object for the containment reference '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getMainActor <em>Main Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.DynamicValueDescription#getMainActor()
	 * @see #getDynamicValueDescription()
	 * @generated
	 */
	EReference getDynamicValueDescription_MainActor();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.Nodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nodes</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Nodes
	 * @generated
	 */
	EClass getNodes();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.Actor#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Actor#getName()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Name();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.MainActor <em>Main Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Main Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.MainActor
	 * @generated
	 */
	EClass getMainActor();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.MainActor#getIdMainActor <em>Id Main Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Main Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.MainActor#getIdMainActor()
	 * @see #getMainActor()
	 * @generated
	 */
	EAttribute getMainActor_IdMainActor();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor <em>Environment Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.EnvironmentActor
	 * @generated
	 */
	EClass getEnvironmentActor();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getHasValueExchange <em>Has Value Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Value Exchange</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.EnvironmentActor#getHasValueExchange()
	 * @see #getEnvironmentActor()
	 * @generated
	 */
	EReference getEnvironmentActor_HasValueExchange();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getIdEnvironmentActor <em>Id Environment Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Environment Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.EnvironmentActor#getIdEnvironmentActor()
	 * @see #getEnvironmentActor()
	 * @generated
	 */
	EAttribute getEnvironmentActor_IdEnvironmentActor();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.dvd.model.dvd.Relation#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Relation#getTo()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_To();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.dvd.model.dvd.Relation#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Relation#getFrom()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_From();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation <em>Who Start Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Who Start Relation</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.WhoStartRelation
	 * @generated
	 */
	EClass getWhoStartRelation();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEForward <em>Has VE Forward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has VE Forward</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEForward()
	 * @see #getWhoStartRelation()
	 * @generated
	 */
	EReference getWhoStartRelation_HasVEForward();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEBackward <em>Has VE Backward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has VE Backward</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEBackward()
	 * @see #getWhoStartRelation()
	 * @generated
	 */
	EReference getWhoStartRelation_HasVEBackward();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.SimpleRelation <em>Simple Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Relation</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.SimpleRelation
	 * @generated
	 */
	EClass getSimpleRelation();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.ValueExchange <em>Value Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Exchange</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange
	 * @generated
	 */
	EClass getValueExchange();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getId()
	 * @see #getValueExchange()
	 * @generated
	 */
	EAttribute getValueExchange_Id();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getDescription()
	 * @see #getValueExchange()
	 * @generated
	 */
	EAttribute getValueExchange_Description();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor <em>Has Environment Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Has Environment Actor</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor()
	 * @see #getValueExchange()
	 * @generated
	 */
	EReference getValueExchange_HasEnvironmentActor();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getInValueObject <em>In Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>In Value Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getInValueObject()
	 * @see #getValueExchange()
	 * @generated
	 */
	EReference getValueExchange_InValueObject();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getOutValueObject <em>Out Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Out Value Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getOutValueObject()
	 * @see #getValueExchange()
	 * @generated
	 */
	EReference getValueExchange_OutValueObject();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.Port#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Port#getObject()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Object();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.Port#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Port#getDescription()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Description();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.Port#getIdObject <em>Id Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.Port#getIdObject()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_IdObject();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.InValueObject <em>In Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Value Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.InValueObject
	 * @generated
	 */
	EClass getInValueObject();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.OutValueObject <em>Out Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Value Object</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.OutValueObject
	 * @generated
	 */
	EClass getOutValueObject();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement <em>Value Level Agreement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Level Agreement</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueLevelAgreement
	 * @generated
	 */
	EClass getValueLevelAgreement();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getDescription()
	 * @see #getValueLevelAgreement()
	 * @generated
	 */
	EAttribute getValueLevelAgreement_Description();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getIdVLA <em>Id VLA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id VLA</em>'.
	 * @see pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getIdVLA()
	 * @see #getValueLevelAgreement()
	 * @generated
	 */
	EAttribute getValueLevelAgreement_IdVLA();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DvdFactory getDvdFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl <em>Dynamic Value Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getDynamicValueDescription()
		 * @generated
		 */
		EClass DYNAMIC_VALUE_DESCRIPTION = eINSTANCE.getDynamicValueDescription();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_VALUE_DESCRIPTION__NODES = eINSTANCE.getDynamicValueDescription_Nodes();

		/**
		 * The meta object literal for the '<em><b>Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_VALUE_DESCRIPTION__RELATIONS = eINSTANCE.getDynamicValueDescription_Relations();

		/**
		 * The meta object literal for the '<em><b>Main Actor</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR = eINSTANCE.getDynamicValueDescription_MainActor();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.NodesImpl <em>Nodes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.NodesImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getNodes()
		 * @generated
		 */
		EClass NODES = eINSTANCE.getNodes();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.ActorImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__NAME = eINSTANCE.getActor_Name();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.MainActorImpl <em>Main Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.MainActorImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getMainActor()
		 * @generated
		 */
		EClass MAIN_ACTOR = eINSTANCE.getMainActor();

		/**
		 * The meta object literal for the '<em><b>Id Main Actor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAIN_ACTOR__ID_MAIN_ACTOR = eINSTANCE.getMainActor_IdMainActor();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl <em>Environment Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.EnvironmentActorImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getEnvironmentActor()
		 * @generated
		 */
		EClass ENVIRONMENT_ACTOR = eINSTANCE.getEnvironmentActor();

		/**
		 * The meta object literal for the '<em><b>Has Value Exchange</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE = eINSTANCE.getEnvironmentActor_HasValueExchange();

		/**
		 * The meta object literal for the '<em><b>Id Environment Actor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR = eINSTANCE.getEnvironmentActor_IdEnvironmentActor();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.RelationImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__TO = eINSTANCE.getRelation_To();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__FROM = eINSTANCE.getRelation_From();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl <em>Who Start Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.WhoStartRelationImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getWhoStartRelation()
		 * @generated
		 */
		EClass WHO_START_RELATION = eINSTANCE.getWhoStartRelation();

		/**
		 * The meta object literal for the '<em><b>Has VE Forward</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHO_START_RELATION__HAS_VE_FORWARD = eINSTANCE.getWhoStartRelation_HasVEForward();

		/**
		 * The meta object literal for the '<em><b>Has VE Backward</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHO_START_RELATION__HAS_VE_BACKWARD = eINSTANCE.getWhoStartRelation_HasVEBackward();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.SimpleRelationImpl <em>Simple Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.SimpleRelationImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getSimpleRelation()
		 * @generated
		 */
		EClass SIMPLE_RELATION = eINSTANCE.getSimpleRelation();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl <em>Value Exchange</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.ValueExchangeImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getValueExchange()
		 * @generated
		 */
		EClass VALUE_EXCHANGE = eINSTANCE.getValueExchange();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_EXCHANGE__ID = eINSTANCE.getValueExchange_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_EXCHANGE__DESCRIPTION = eINSTANCE.getValueExchange_Description();

		/**
		 * The meta object literal for the '<em><b>Has Environment Actor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR = eINSTANCE.getValueExchange_HasEnvironmentActor();

		/**
		 * The meta object literal for the '<em><b>In Value Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_EXCHANGE__IN_VALUE_OBJECT = eINSTANCE.getValueExchange_InValueObject();

		/**
		 * The meta object literal for the '<em><b>Out Value Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_EXCHANGE__OUT_VALUE_OBJECT = eINSTANCE.getValueExchange_OutValueObject();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.PortImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__OBJECT = eINSTANCE.getPort_Object();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__DESCRIPTION = eINSTANCE.getPort_Description();

		/**
		 * The meta object literal for the '<em><b>Id Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__ID_OBJECT = eINSTANCE.getPort_IdObject();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.InValueObjectImpl <em>In Value Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.InValueObjectImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getInValueObject()
		 * @generated
		 */
		EClass IN_VALUE_OBJECT = eINSTANCE.getInValueObject();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.OutValueObjectImpl <em>Out Value Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.OutValueObjectImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getOutValueObject()
		 * @generated
		 */
		EClass OUT_VALUE_OBJECT = eINSTANCE.getOutValueObject();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl <em>Value Level Agreement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl
		 * @see pt.unl.fct.dvd.model.dvd.impl.DvdPackageImpl#getValueLevelAgreement()
		 * @generated
		 */
		EClass VALUE_LEVEL_AGREEMENT = eINSTANCE.getValueLevelAgreement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_LEVEL_AGREEMENT__DESCRIPTION = eINSTANCE.getValueLevelAgreement_Description();

		/**
		 * The meta object literal for the '<em><b>Id VLA</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_LEVEL_AGREEMENT__ID_VLA = eINSTANCE.getValueLevelAgreement_IdVLA();

	}

} //DvdPackage
