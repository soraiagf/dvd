/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Level Agreement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getIdVLA <em>Id VLA</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueLevelAgreement()
 * @model
 * @generated
 */
public interface ValueLevelAgreement extends Nodes {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueLevelAgreement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Id VLA</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id VLA</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id VLA</em>' attribute.
	 * @see #setIdVLA(int)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getValueLevelAgreement_IdVLA()
	 * @model required="true"
	 * @generated
	 */
	int getIdVLA();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement#getIdVLA <em>Id VLA</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id VLA</em>' attribute.
	 * @see #getIdVLA()
	 * @generated
	 */
	void setIdVLA(int value);

} // ValueLevelAgreement
