/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EClass;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.SimpleRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimpleRelationImpl extends RelationImpl implements SimpleRelation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.SIMPLE_RELATION;
	}

} //SimpleRelationImpl
