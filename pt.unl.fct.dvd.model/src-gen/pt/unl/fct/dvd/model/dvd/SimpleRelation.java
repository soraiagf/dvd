/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getSimpleRelation()
 * @model
 * @generated
 */
public interface SimpleRelation extends Relation {
} // SimpleRelation
