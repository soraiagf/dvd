/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getHasValueExchange <em>Has Value Exchange</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getIdEnvironmentActor <em>Id Environment Actor</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getEnvironmentActor()
 * @model
 * @generated
 */
public interface EnvironmentActor extends Actor {
	/**
	 * Returns the value of the '<em><b>Has Value Exchange</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.dvd.model.dvd.ValueExchange}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor <em>Has Environment Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Value Exchange</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Value Exchange</em>' reference list.
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getEnvironmentActor_HasValueExchange()
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange#getHasEnvironmentActor
	 * @model opposite="hasEnvironmentActor" keys="id" required="true"
	 * @generated
	 */
	EList<ValueExchange> getHasValueExchange();

	/**
	 * Returns the value of the '<em><b>Id Environment Actor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Environment Actor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Environment Actor</em>' attribute.
	 * @see #setIdEnvironmentActor(int)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getEnvironmentActor_IdEnvironmentActor()
	 * @model required="true"
	 * @generated
	 */
	int getIdEnvironmentActor();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor#getIdEnvironmentActor <em>Id Environment Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Environment Actor</em>' attribute.
	 * @see #getIdEnvironmentActor()
	 * @generated
	 */
	void setIdEnvironmentActor(int value);

} // EnvironmentActor
