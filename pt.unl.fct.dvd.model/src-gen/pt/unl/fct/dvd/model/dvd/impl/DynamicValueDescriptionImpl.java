/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.DynamicValueDescription;
import pt.unl.fct.dvd.model.dvd.MainActor;
import pt.unl.fct.dvd.model.dvd.Nodes;
import pt.unl.fct.dvd.model.dvd.Relation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dynamic Value Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl#getRelations <em>Relations</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.DynamicValueDescriptionImpl#getMainActor <em>Main Actor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DynamicValueDescriptionImpl extends MinimalEObjectImpl.Container implements DynamicValueDescription {
	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<Nodes> nodes;

	/**
	 * The cached value of the '{@link #getRelations() <em>Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<Relation> relations;

	/**
	 * The cached value of the '{@link #getMainActor() <em>Main Actor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainActor()
	 * @generated
	 * @ordered
	 */
	protected MainActor mainActor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DynamicValueDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.DYNAMIC_VALUE_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Nodes> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentEList<Nodes>(Nodes.class, this, DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Relation> getRelations() {
		if (relations == null) {
			relations = new EObjectContainmentEList<Relation>(Relation.class, this,
					DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS);
		}
		return relations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MainActor getMainActor() {
		return mainActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainActor(MainActor newMainActor, NotificationChain msgs) {
		MainActor oldMainActor = mainActor;
		mainActor = newMainActor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR, oldMainActor, newMainActor);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainActor(MainActor newMainActor) {
		if (newMainActor != mainActor) {
			NotificationChain msgs = null;
			if (mainActor != null)
				msgs = ((InternalEObject) mainActor).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR, null, msgs);
			if (newMainActor != null)
				msgs = ((InternalEObject) newMainActor).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR, null, msgs);
			msgs = basicSetMainActor(newMainActor, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR,
					newMainActor, newMainActor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES:
			return ((InternalEList<?>) getNodes()).basicRemove(otherEnd, msgs);
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS:
			return ((InternalEList<?>) getRelations()).basicRemove(otherEnd, msgs);
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR:
			return basicSetMainActor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES:
			return getNodes();
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS:
			return getRelations();
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR:
			return getMainActor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES:
			getNodes().clear();
			getNodes().addAll((Collection<? extends Nodes>) newValue);
			return;
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS:
			getRelations().clear();
			getRelations().addAll((Collection<? extends Relation>) newValue);
			return;
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR:
			setMainActor((MainActor) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES:
			getNodes().clear();
			return;
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS:
			getRelations().clear();
			return;
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR:
			setMainActor((MainActor) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__NODES:
			return nodes != null && !nodes.isEmpty();
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__RELATIONS:
			return relations != null && !relations.isEmpty();
		case DvdPackage.DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR:
			return mainActor != null;
		}
		return super.eIsSet(featureID);
	}

} //DynamicValueDescriptionImpl
