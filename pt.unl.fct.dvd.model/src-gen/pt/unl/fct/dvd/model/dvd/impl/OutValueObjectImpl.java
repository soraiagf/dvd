/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EClass;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.OutValueObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Value Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OutValueObjectImpl extends PortImpl implements OutValueObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutValueObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.OUT_VALUE_OBJECT;
	}

} //OutValueObjectImpl
