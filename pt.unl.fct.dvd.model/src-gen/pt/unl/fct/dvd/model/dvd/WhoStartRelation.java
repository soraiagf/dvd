/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Who Start Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEForward <em>Has VE Forward</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation#getHasVEBackward <em>Has VE Backward</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getWhoStartRelation()
 * @model
 * @generated
 */
public interface WhoStartRelation extends Relation {
	/**
	 * Returns the value of the '<em><b>Has VE Forward</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.dvd.model.dvd.ValueExchange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has VE Forward</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has VE Forward</em>' reference list.
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getWhoStartRelation_HasVEForward()
	 * @model
	 * @generated
	 */
	EList<ValueExchange> getHasVEForward();

	/**
	 * Returns the value of the '<em><b>Has VE Backward</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.dvd.model.dvd.ValueExchange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has VE Backward</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has VE Backward</em>' reference list.
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getWhoStartRelation_HasVEBackward()
	 * @model
	 * @generated
	 */
	EList<ValueExchange> getHasVEBackward();

} // WhoStartRelation
