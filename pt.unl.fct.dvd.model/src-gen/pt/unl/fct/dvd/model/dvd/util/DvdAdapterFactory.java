/**
 */
package pt.unl.fct.dvd.model.dvd.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import pt.unl.fct.dvd.model.dvd.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage
 * @generated
 */
public class DvdAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DvdPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DvdAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DvdPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DvdSwitch<Adapter> modelSwitch = new DvdSwitch<Adapter>() {
		@Override
		public Adapter caseDynamicValueDescription(DynamicValueDescription object) {
			return createDynamicValueDescriptionAdapter();
		}

		@Override
		public Adapter caseNodes(Nodes object) {
			return createNodesAdapter();
		}

		@Override
		public Adapter caseActor(Actor object) {
			return createActorAdapter();
		}

		@Override
		public Adapter caseMainActor(MainActor object) {
			return createMainActorAdapter();
		}

		@Override
		public Adapter caseEnvironmentActor(EnvironmentActor object) {
			return createEnvironmentActorAdapter();
		}

		@Override
		public Adapter caseRelation(Relation object) {
			return createRelationAdapter();
		}

		@Override
		public Adapter caseWhoStartRelation(WhoStartRelation object) {
			return createWhoStartRelationAdapter();
		}

		@Override
		public Adapter caseSimpleRelation(SimpleRelation object) {
			return createSimpleRelationAdapter();
		}

		@Override
		public Adapter caseValueExchange(ValueExchange object) {
			return createValueExchangeAdapter();
		}

		@Override
		public Adapter casePort(Port object) {
			return createPortAdapter();
		}

		@Override
		public Adapter caseInValueObject(InValueObject object) {
			return createInValueObjectAdapter();
		}

		@Override
		public Adapter caseOutValueObject(OutValueObject object) {
			return createOutValueObjectAdapter();
		}

		@Override
		public Adapter caseValueLevelAgreement(ValueLevelAgreement object) {
			return createValueLevelAgreementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.DynamicValueDescription <em>Dynamic Value Description</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.DynamicValueDescription
	 * @generated
	 */
	public Adapter createDynamicValueDescriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.Nodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.Nodes
	 * @generated
	 */
	public Adapter createNodesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.Actor
	 * @generated
	 */
	public Adapter createActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.MainActor <em>Main Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.MainActor
	 * @generated
	 */
	public Adapter createMainActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.EnvironmentActor <em>Environment Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.EnvironmentActor
	 * @generated
	 */
	public Adapter createEnvironmentActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.Relation
	 * @generated
	 */
	public Adapter createRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.WhoStartRelation <em>Who Start Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.WhoStartRelation
	 * @generated
	 */
	public Adapter createWhoStartRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.SimpleRelation <em>Simple Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.SimpleRelation
	 * @generated
	 */
	public Adapter createSimpleRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.ValueExchange <em>Value Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.ValueExchange
	 * @generated
	 */
	public Adapter createValueExchangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.Port
	 * @generated
	 */
	public Adapter createPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.InValueObject <em>In Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.InValueObject
	 * @generated
	 */
	public Adapter createInValueObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.OutValueObject <em>Out Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.OutValueObject
	 * @generated
	 */
	public Adapter createOutValueObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link pt.unl.fct.dvd.model.dvd.ValueLevelAgreement <em>Value Level Agreement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see pt.unl.fct.dvd.model.dvd.ValueLevelAgreement
	 * @generated
	 */
	public Adapter createValueLevelAgreementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DvdAdapterFactory
