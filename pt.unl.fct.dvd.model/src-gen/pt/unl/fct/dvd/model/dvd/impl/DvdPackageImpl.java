/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import pt.unl.fct.dvd.model.dvd.Actor;
import pt.unl.fct.dvd.model.dvd.DvdFactory;
import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.DynamicValueDescription;
import pt.unl.fct.dvd.model.dvd.EnvironmentActor;
import pt.unl.fct.dvd.model.dvd.InValueObject;
import pt.unl.fct.dvd.model.dvd.MainActor;
import pt.unl.fct.dvd.model.dvd.Nodes;
import pt.unl.fct.dvd.model.dvd.OutValueObject;
import pt.unl.fct.dvd.model.dvd.Port;
import pt.unl.fct.dvd.model.dvd.Relation;
import pt.unl.fct.dvd.model.dvd.SimpleRelation;
import pt.unl.fct.dvd.model.dvd.ValueExchange;
import pt.unl.fct.dvd.model.dvd.ValueLevelAgreement;
import pt.unl.fct.dvd.model.dvd.WhoStartRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DvdPackageImpl extends EPackageImpl implements DvdPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicValueDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mainActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whoStartRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueExchangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inValueObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outValueObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueLevelAgreementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DvdPackageImpl() {
		super(eNS_URI, DvdFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DvdPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DvdPackage init() {
		if (isInited)
			return (DvdPackage) EPackage.Registry.INSTANCE.getEPackage(DvdPackage.eNS_URI);

		// Obtain or create and register package
		DvdPackageImpl theDvdPackage = (DvdPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof DvdPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new DvdPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theDvdPackage.createPackageContents();

		// Initialize created meta-data
		theDvdPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDvdPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DvdPackage.eNS_URI, theDvdPackage);
		return theDvdPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDynamicValueDescription() {
		return dynamicValueDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDynamicValueDescription_Nodes() {
		return (EReference) dynamicValueDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDynamicValueDescription_Relations() {
		return (EReference) dynamicValueDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDynamicValueDescription_MainActor() {
		return (EReference) dynamicValueDescriptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodes() {
		return nodesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActor() {
		return actorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActor_Name() {
		return (EAttribute) actorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMainActor() {
		return mainActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMainActor_IdMainActor() {
		return (EAttribute) mainActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironmentActor() {
		return environmentActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironmentActor_HasValueExchange() {
		return (EReference) environmentActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironmentActor_IdEnvironmentActor() {
		return (EAttribute) environmentActorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelation() {
		return relationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelation_To() {
		return (EReference) relationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelation_From() {
		return (EReference) relationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhoStartRelation() {
		return whoStartRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhoStartRelation_HasVEForward() {
		return (EReference) whoStartRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhoStartRelation_HasVEBackward() {
		return (EReference) whoStartRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleRelation() {
		return simpleRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueExchange() {
		return valueExchangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueExchange_Id() {
		return (EAttribute) valueExchangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueExchange_Description() {
		return (EAttribute) valueExchangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueExchange_HasEnvironmentActor() {
		return (EReference) valueExchangeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueExchange_InValueObject() {
		return (EReference) valueExchangeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueExchange_OutValueObject() {
		return (EReference) valueExchangeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort() {
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Object() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Description() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_IdObject() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInValueObject() {
		return inValueObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutValueObject() {
		return outValueObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueLevelAgreement() {
		return valueLevelAgreementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueLevelAgreement_Description() {
		return (EAttribute) valueLevelAgreementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueLevelAgreement_IdVLA() {
		return (EAttribute) valueLevelAgreementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DvdFactory getDvdFactory() {
		return (DvdFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		dynamicValueDescriptionEClass = createEClass(DYNAMIC_VALUE_DESCRIPTION);
		createEReference(dynamicValueDescriptionEClass, DYNAMIC_VALUE_DESCRIPTION__NODES);
		createEReference(dynamicValueDescriptionEClass, DYNAMIC_VALUE_DESCRIPTION__RELATIONS);
		createEReference(dynamicValueDescriptionEClass, DYNAMIC_VALUE_DESCRIPTION__MAIN_ACTOR);

		nodesEClass = createEClass(NODES);

		actorEClass = createEClass(ACTOR);
		createEAttribute(actorEClass, ACTOR__NAME);

		mainActorEClass = createEClass(MAIN_ACTOR);
		createEAttribute(mainActorEClass, MAIN_ACTOR__ID_MAIN_ACTOR);

		environmentActorEClass = createEClass(ENVIRONMENT_ACTOR);
		createEReference(environmentActorEClass, ENVIRONMENT_ACTOR__HAS_VALUE_EXCHANGE);
		createEAttribute(environmentActorEClass, ENVIRONMENT_ACTOR__ID_ENVIRONMENT_ACTOR);

		relationEClass = createEClass(RELATION);
		createEReference(relationEClass, RELATION__TO);
		createEReference(relationEClass, RELATION__FROM);

		whoStartRelationEClass = createEClass(WHO_START_RELATION);
		createEReference(whoStartRelationEClass, WHO_START_RELATION__HAS_VE_FORWARD);
		createEReference(whoStartRelationEClass, WHO_START_RELATION__HAS_VE_BACKWARD);

		simpleRelationEClass = createEClass(SIMPLE_RELATION);

		valueExchangeEClass = createEClass(VALUE_EXCHANGE);
		createEAttribute(valueExchangeEClass, VALUE_EXCHANGE__ID);
		createEAttribute(valueExchangeEClass, VALUE_EXCHANGE__DESCRIPTION);
		createEReference(valueExchangeEClass, VALUE_EXCHANGE__HAS_ENVIRONMENT_ACTOR);
		createEReference(valueExchangeEClass, VALUE_EXCHANGE__IN_VALUE_OBJECT);
		createEReference(valueExchangeEClass, VALUE_EXCHANGE__OUT_VALUE_OBJECT);

		portEClass = createEClass(PORT);
		createEAttribute(portEClass, PORT__OBJECT);
		createEAttribute(portEClass, PORT__DESCRIPTION);
		createEAttribute(portEClass, PORT__ID_OBJECT);

		inValueObjectEClass = createEClass(IN_VALUE_OBJECT);

		outValueObjectEClass = createEClass(OUT_VALUE_OBJECT);

		valueLevelAgreementEClass = createEClass(VALUE_LEVEL_AGREEMENT);
		createEAttribute(valueLevelAgreementEClass, VALUE_LEVEL_AGREEMENT__DESCRIPTION);
		createEAttribute(valueLevelAgreementEClass, VALUE_LEVEL_AGREEMENT__ID_VLA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		actorEClass.getESuperTypes().add(this.getNodes());
		mainActorEClass.getESuperTypes().add(this.getActor());
		environmentActorEClass.getESuperTypes().add(this.getActor());
		whoStartRelationEClass.getESuperTypes().add(this.getRelation());
		simpleRelationEClass.getESuperTypes().add(this.getRelation());
		valueExchangeEClass.getESuperTypes().add(this.getNodes());
		portEClass.getESuperTypes().add(this.getNodes());
		inValueObjectEClass.getESuperTypes().add(this.getPort());
		outValueObjectEClass.getESuperTypes().add(this.getPort());
		valueLevelAgreementEClass.getESuperTypes().add(this.getNodes());

		// Initialize classes, features, and operations; add parameters
		initEClass(dynamicValueDescriptionEClass, DynamicValueDescription.class, "DynamicValueDescription",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDynamicValueDescription_Nodes(), this.getNodes(), null, "nodes", null, 1, -1,
				DynamicValueDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDynamicValueDescription_Relations(), this.getRelation(), null, "relations", null, 1, -1,
				DynamicValueDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDynamicValueDescription_MainActor(), this.getMainActor(), null, "mainActor", null, 1, 1,
				DynamicValueDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodesEClass, Nodes.class, "Nodes", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(actorEClass, Actor.class, "Actor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActor_Name(), ecorePackage.getEString(), "name", null, 0, 1, Actor.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mainActorEClass, MainActor.class, "MainActor", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMainActor_IdMainActor(), ecorePackage.getEInt(), "idMainActor", null, 1, 1, MainActor.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentActorEClass, EnvironmentActor.class, "EnvironmentActor", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnvironmentActor_HasValueExchange(), this.getValueExchange(),
				this.getValueExchange_HasEnvironmentActor(), "hasValueExchange", null, 1, -1, EnvironmentActor.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getEnvironmentActor_HasValueExchange().getEKeys().add(this.getValueExchange_Id());
		initEAttribute(getEnvironmentActor_IdEnvironmentActor(), ecorePackage.getEInt(), "idEnvironmentActor", null, 1,
				1, EnvironmentActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationEClass, Relation.class, "Relation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelation_To(), this.getNodes(), null, "to", null, 1, 1, Relation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getRelation_From(), this.getNodes(), null, "from", null, 1, 1, Relation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(whoStartRelationEClass, WhoStartRelation.class, "WhoStartRelation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWhoStartRelation_HasVEForward(), this.getValueExchange(), null, "hasVEForward", null, 0, -1,
				WhoStartRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhoStartRelation_HasVEBackward(), this.getValueExchange(), null, "hasVEBackward", null, 0, -1,
				WhoStartRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleRelationEClass, SimpleRelation.class, "SimpleRelation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(valueExchangeEClass, ValueExchange.class, "ValueExchange", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueExchange_Id(), ecorePackage.getEInt(), "id", null, 1, 1, ValueExchange.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getValueExchange_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ValueExchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getValueExchange_HasEnvironmentActor(), this.getEnvironmentActor(),
				this.getEnvironmentActor_HasValueExchange(), "hasEnvironmentActor", null, 1, 1, ValueExchange.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getValueExchange_InValueObject(), this.getInValueObject(), null, "InValueObject", null, 1, 1,
				ValueExchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getValueExchange_OutValueObject(), this.getOutValueObject(), null, "OutValueObject", null, 1, 1,
				ValueExchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPort_Object(), ecorePackage.getEString(), "object", null, 0, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Description(), ecorePackage.getEString(), "description", null, 0, 1, Port.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_IdObject(), ecorePackage.getEInt(), "idObject", null, 1, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inValueObjectEClass, InValueObject.class, "InValueObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(outValueObjectEClass, OutValueObject.class, "OutValueObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(valueLevelAgreementEClass, ValueLevelAgreement.class, "ValueLevelAgreement", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueLevelAgreement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ValueLevelAgreement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getValueLevelAgreement_IdVLA(), ecorePackage.getEInt(), "idVLA", null, 1, 1,
				ValueLevelAgreement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DvdPackageImpl
