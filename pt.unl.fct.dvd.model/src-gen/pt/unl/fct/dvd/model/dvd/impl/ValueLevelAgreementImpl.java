/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.ValueLevelAgreement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Level Agreement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.ValueLevelAgreementImpl#getIdVLA <em>Id VLA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueLevelAgreementImpl extends NodesImpl implements ValueLevelAgreement {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdVLA() <em>Id VLA</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdVLA()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_VLA_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIdVLA() <em>Id VLA</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdVLA()
	 * @generated
	 * @ordered
	 */
	protected int idVLA = ID_VLA_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueLevelAgreementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.VALUE_LEVEL_AGREEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_LEVEL_AGREEMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIdVLA() {
		return idVLA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdVLA(int newIdVLA) {
		int oldIdVLA = idVLA;
		idVLA = newIdVLA;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.VALUE_LEVEL_AGREEMENT__ID_VLA, oldIdVLA,
					idVLA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.VALUE_LEVEL_AGREEMENT__DESCRIPTION:
			return getDescription();
		case DvdPackage.VALUE_LEVEL_AGREEMENT__ID_VLA:
			return getIdVLA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.VALUE_LEVEL_AGREEMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case DvdPackage.VALUE_LEVEL_AGREEMENT__ID_VLA:
			setIdVLA((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.VALUE_LEVEL_AGREEMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case DvdPackage.VALUE_LEVEL_AGREEMENT__ID_VLA:
			setIdVLA(ID_VLA_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.VALUE_LEVEL_AGREEMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case DvdPackage.VALUE_LEVEL_AGREEMENT__ID_VLA:
			return idVLA != ID_VLA_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", idVLA: ");
		result.append(idVLA);
		result.append(')');
		return result.toString();
	}

} //ValueLevelAgreementImpl
