/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.MainActor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Main Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.impl.MainActorImpl#getIdMainActor <em>Id Main Actor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MainActorImpl extends ActorImpl implements MainActor {
	/**
	 * The default value of the '{@link #getIdMainActor() <em>Id Main Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdMainActor()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_MAIN_ACTOR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIdMainActor() <em>Id Main Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdMainActor()
	 * @generated
	 * @ordered
	 */
	protected int idMainActor = ID_MAIN_ACTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MainActorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.MAIN_ACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIdMainActor() {
		return idMainActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdMainActor(int newIdMainActor) {
		int oldIdMainActor = idMainActor;
		idMainActor = newIdMainActor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DvdPackage.MAIN_ACTOR__ID_MAIN_ACTOR, oldIdMainActor,
					idMainActor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DvdPackage.MAIN_ACTOR__ID_MAIN_ACTOR:
			return getIdMainActor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DvdPackage.MAIN_ACTOR__ID_MAIN_ACTOR:
			setIdMainActor((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DvdPackage.MAIN_ACTOR__ID_MAIN_ACTOR:
			setIdMainActor(ID_MAIN_ACTOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DvdPackage.MAIN_ACTOR__ID_MAIN_ACTOR:
			return idMainActor != ID_MAIN_ACTOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (idMainActor: ");
		result.append(idMainActor);
		result.append(')');
		return result.toString();
	}

} //MainActorImpl
