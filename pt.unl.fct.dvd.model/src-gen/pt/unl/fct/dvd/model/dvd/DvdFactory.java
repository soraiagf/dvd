/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage
 * @generated
 */
public interface DvdFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DvdFactory eINSTANCE = pt.unl.fct.dvd.model.dvd.impl.DvdFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dynamic Value Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dynamic Value Description</em>'.
	 * @generated
	 */
	DynamicValueDescription createDynamicValueDescription();

	/**
	 * Returns a new object of class '<em>Main Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Main Actor</em>'.
	 * @generated
	 */
	MainActor createMainActor();

	/**
	 * Returns a new object of class '<em>Environment Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Environment Actor</em>'.
	 * @generated
	 */
	EnvironmentActor createEnvironmentActor();

	/**
	 * Returns a new object of class '<em>Who Start Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Who Start Relation</em>'.
	 * @generated
	 */
	WhoStartRelation createWhoStartRelation();

	/**
	 * Returns a new object of class '<em>Simple Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Relation</em>'.
	 * @generated
	 */
	SimpleRelation createSimpleRelation();

	/**
	 * Returns a new object of class '<em>Value Exchange</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Exchange</em>'.
	 * @generated
	 */
	ValueExchange createValueExchange();

	/**
	 * Returns a new object of class '<em>In Value Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>In Value Object</em>'.
	 * @generated
	 */
	InValueObject createInValueObject();

	/**
	 * Returns a new object of class '<em>Out Value Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Out Value Object</em>'.
	 * @generated
	 */
	OutValueObject createOutValueObject();

	/**
	 * Returns a new object of class '<em>Value Level Agreement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Level Agreement</em>'.
	 * @generated
	 */
	ValueLevelAgreement createValueLevelAgreement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DvdPackage getDvdPackage();

} //DvdFactory
