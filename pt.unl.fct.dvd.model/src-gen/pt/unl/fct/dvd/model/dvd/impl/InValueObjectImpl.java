/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EClass;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.InValueObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Value Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InValueObjectImpl extends PortImpl implements InValueObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InValueObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.IN_VALUE_OBJECT;
	}

} //InValueObjectImpl
