/**
 */
package pt.unl.fct.dvd.model.dvd;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.Relation#getTo <em>To</em>}</li>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.Relation#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getRelation()
 * @model abstract="true"
 * @generated
 */
public interface Relation extends EObject {
	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(Nodes)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getRelation_To()
	 * @model required="true"
	 * @generated
	 */
	Nodes getTo();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.Relation#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Nodes value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(Nodes)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getRelation_From()
	 * @model required="true"
	 * @generated
	 */
	Nodes getFrom();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.Relation#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Nodes value);

} // Relation
