/**
 */
package pt.unl.fct.dvd.model.dvd.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pt.unl.fct.dvd.model.dvd.DvdPackage;
import pt.unl.fct.dvd.model.dvd.Nodes;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nodes</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class NodesImpl extends MinimalEObjectImpl.Container implements Nodes {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DvdPackage.Literals.NODES;
	}

} //NodesImpl
