/**
 */
package pt.unl.fct.dvd.model.dvd;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Main Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.dvd.model.dvd.MainActor#getIdMainActor <em>Id Main Actor</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getMainActor()
 * @model
 * @generated
 */
public interface MainActor extends Actor {
	/**
	 * Returns the value of the '<em><b>Id Main Actor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Main Actor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Main Actor</em>' attribute.
	 * @see #setIdMainActor(int)
	 * @see pt.unl.fct.dvd.model.dvd.DvdPackage#getMainActor_IdMainActor()
	 * @model required="true"
	 * @generated
	 */
	int getIdMainActor();

	/**
	 * Sets the value of the '{@link pt.unl.fct.dvd.model.dvd.MainActor#getIdMainActor <em>Id Main Actor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Main Actor</em>' attribute.
	 * @see #getIdMainActor()
	 * @generated
	 */
	void setIdMainActor(int value);

} // MainActor
