/**
 */
package SoaML;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see SoaML.SoaMLFactory
 * @model kind="package"
 * @generated
 */
public interface SoaMLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "SoaML";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "pt.unl.fct.SoaML";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "pt.unl.fct.SoaML";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SoaMLPackage eINSTANCE = SoaML.impl.SoaMLPackageImpl.init();

	/**
	 * The meta object id for the '{@link SoaML.impl.SoaMLImpl <em>Soa ML</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.SoaMLImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getSoaML()
	 * @generated
	 */
	int SOA_ML = 0;

	/**
	 * The feature id for the '<em><b>Services Architecture</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOA_ML__SERVICES_ARCHITECTURE = 0;

	/**
	 * The number of structural features of the '<em>Soa ML</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOA_ML_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Soa ML</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOA_ML_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SoaML.impl.ServicesArchitectureImpl <em>Services Architecture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.ServicesArchitectureImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getServicesArchitecture()
	 * @generated
	 */
	int SERVICES_ARCHITECTURE = 1;

	/**
	 * The feature id for the '<em><b>Service Contract</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_ARCHITECTURE__SERVICE_CONTRACT = 0;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_ARCHITECTURE__PARTICIPANT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_ARCHITECTURE__NAME = 2;

	/**
	 * The number of structural features of the '<em>Services Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_ARCHITECTURE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Services Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_ARCHITECTURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SoaML.impl.ParticipantImpl <em>Participant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.ParticipantImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getParticipant()
	 * @generated
	 */
	int PARTICIPANT = 2;

	/**
	 * The feature id for the '<em><b>Service Contract</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__SERVICE_CONTRACT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SoaML.impl.ServiceContractImpl <em>Service Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.ServiceContractImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getServiceContract()
	 * @generated
	 */
	int SERVICE_CONTRACT = 3;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT__PARTICIPANT = 0;

	/**
	 * The feature id for the '<em><b>Has Service Contract</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT__HAS_SERVICE_CONTRACT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT__NAME = 2;

	/**
	 * The feature id for the '<em><b>Has Candidate Services</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES = 3;

	/**
	 * The feature id for the '<em><b>Candidate Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT__CANDIDATE_SERVICES = 4;

	/**
	 * The number of structural features of the '<em>Service Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Service Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CONTRACT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SoaML.impl.CandidateServicesImpl <em>Candidate Services</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.CandidateServicesImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getCandidateServices()
	 * @generated
	 */
	int CANDIDATE_SERVICES = 4;

	/**
	 * The feature id for the '<em><b>Has Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CANDIDATE_SERVICES__HAS_OPERATION = 0;

	/**
	 * The number of structural features of the '<em>Candidate Services</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CANDIDATE_SERVICES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Candidate Services</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CANDIDATE_SERVICES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SoaML.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SoaML.impl.OperationImpl
	 * @see SoaML.impl.SoaMLPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Has Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__HAS_OPERATION = CANDIDATE_SERVICES__HAS_OPERATION;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OPERATION = CANDIDATE_SERVICES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = CANDIDATE_SERVICES_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = CANDIDATE_SERVICES_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link SoaML.SoaML <em>Soa ML</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soa ML</em>'.
	 * @see SoaML.SoaML
	 * @generated
	 */
	EClass getSoaML();

	/**
	 * Returns the meta object for the containment reference '{@link SoaML.SoaML#getServicesArchitecture <em>Services Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Services Architecture</em>'.
	 * @see SoaML.SoaML#getServicesArchitecture()
	 * @see #getSoaML()
	 * @generated
	 */
	EReference getSoaML_ServicesArchitecture();

	/**
	 * Returns the meta object for class '{@link SoaML.ServicesArchitecture <em>Services Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Services Architecture</em>'.
	 * @see SoaML.ServicesArchitecture
	 * @generated
	 */
	EClass getServicesArchitecture();

	/**
	 * Returns the meta object for the containment reference list '{@link SoaML.ServicesArchitecture#getServiceContract <em>Service Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Service Contract</em>'.
	 * @see SoaML.ServicesArchitecture#getServiceContract()
	 * @see #getServicesArchitecture()
	 * @generated
	 */
	EReference getServicesArchitecture_ServiceContract();

	/**
	 * Returns the meta object for the containment reference list '{@link SoaML.ServicesArchitecture#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Participant</em>'.
	 * @see SoaML.ServicesArchitecture#getParticipant()
	 * @see #getServicesArchitecture()
	 * @generated
	 */
	EReference getServicesArchitecture_Participant();

	/**
	 * Returns the meta object for the attribute '{@link SoaML.ServicesArchitecture#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see SoaML.ServicesArchitecture#getName()
	 * @see #getServicesArchitecture()
	 * @generated
	 */
	EAttribute getServicesArchitecture_Name();

	/**
	 * Returns the meta object for class '{@link SoaML.Participant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Participant</em>'.
	 * @see SoaML.Participant
	 * @generated
	 */
	EClass getParticipant();

	/**
	 * Returns the meta object for the reference list '{@link SoaML.Participant#getServiceContract <em>Service Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Service Contract</em>'.
	 * @see SoaML.Participant#getServiceContract()
	 * @see #getParticipant()
	 * @generated
	 */
	EReference getParticipant_ServiceContract();

	/**
	 * Returns the meta object for the attribute '{@link SoaML.Participant#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see SoaML.Participant#getName()
	 * @see #getParticipant()
	 * @generated
	 */
	EAttribute getParticipant_Name();

	/**
	 * Returns the meta object for class '{@link SoaML.ServiceContract <em>Service Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Contract</em>'.
	 * @see SoaML.ServiceContract
	 * @generated
	 */
	EClass getServiceContract();

	/**
	 * Returns the meta object for the reference '{@link SoaML.ServiceContract#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Participant</em>'.
	 * @see SoaML.ServiceContract#getParticipant()
	 * @see #getServiceContract()
	 * @generated
	 */
	EReference getServiceContract_Participant();

	/**
	 * Returns the meta object for the reference list '{@link SoaML.ServiceContract#getHasServiceContract <em>Has Service Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Service Contract</em>'.
	 * @see SoaML.ServiceContract#getHasServiceContract()
	 * @see #getServiceContract()
	 * @generated
	 */
	EReference getServiceContract_HasServiceContract();

	/**
	 * Returns the meta object for the attribute '{@link SoaML.ServiceContract#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see SoaML.ServiceContract#getName()
	 * @see #getServiceContract()
	 * @generated
	 */
	EAttribute getServiceContract_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link SoaML.ServiceContract#getHasCandidateServices <em>Has Candidate Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Candidate Services</em>'.
	 * @see SoaML.ServiceContract#getHasCandidateServices()
	 * @see #getServiceContract()
	 * @generated
	 */
	EReference getServiceContract_HasCandidateServices();

	/**
	 * Returns the meta object for the reference list '{@link SoaML.ServiceContract#getCandidateServices <em>Candidate Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Candidate Services</em>'.
	 * @see SoaML.ServiceContract#getCandidateServices()
	 * @see #getServiceContract()
	 * @generated
	 */
	EReference getServiceContract_CandidateServices();

	/**
	 * Returns the meta object for class '{@link SoaML.CandidateServices <em>Candidate Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Candidate Services</em>'.
	 * @see SoaML.CandidateServices
	 * @generated
	 */
	EClass getCandidateServices();

	/**
	 * Returns the meta object for the containment reference list '{@link SoaML.CandidateServices#getHasOperation <em>Has Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Operation</em>'.
	 * @see SoaML.CandidateServices#getHasOperation()
	 * @see #getCandidateServices()
	 * @generated
	 */
	EReference getCandidateServices_HasOperation();

	/**
	 * Returns the meta object for class '{@link SoaML.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see SoaML.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link SoaML.Operation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see SoaML.Operation#getOperation()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Operation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SoaMLFactory getSoaMLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link SoaML.impl.SoaMLImpl <em>Soa ML</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.SoaMLImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getSoaML()
		 * @generated
		 */
		EClass SOA_ML = eINSTANCE.getSoaML();

		/**
		 * The meta object literal for the '<em><b>Services Architecture</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOA_ML__SERVICES_ARCHITECTURE = eINSTANCE.getSoaML_ServicesArchitecture();

		/**
		 * The meta object literal for the '{@link SoaML.impl.ServicesArchitectureImpl <em>Services Architecture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.ServicesArchitectureImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getServicesArchitecture()
		 * @generated
		 */
		EClass SERVICES_ARCHITECTURE = eINSTANCE.getServicesArchitecture();

		/**
		 * The meta object literal for the '<em><b>Service Contract</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICES_ARCHITECTURE__SERVICE_CONTRACT = eINSTANCE.getServicesArchitecture_ServiceContract();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICES_ARCHITECTURE__PARTICIPANT = eINSTANCE.getServicesArchitecture_Participant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICES_ARCHITECTURE__NAME = eINSTANCE.getServicesArchitecture_Name();

		/**
		 * The meta object literal for the '{@link SoaML.impl.ParticipantImpl <em>Participant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.ParticipantImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getParticipant()
		 * @generated
		 */
		EClass PARTICIPANT = eINSTANCE.getParticipant();

		/**
		 * The meta object literal for the '<em><b>Service Contract</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTICIPANT__SERVICE_CONTRACT = eINSTANCE.getParticipant_ServiceContract();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARTICIPANT__NAME = eINSTANCE.getParticipant_Name();

		/**
		 * The meta object literal for the '{@link SoaML.impl.ServiceContractImpl <em>Service Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.ServiceContractImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getServiceContract()
		 * @generated
		 */
		EClass SERVICE_CONTRACT = eINSTANCE.getServiceContract();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CONTRACT__PARTICIPANT = eINSTANCE.getServiceContract_Participant();

		/**
		 * The meta object literal for the '<em><b>Has Service Contract</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CONTRACT__HAS_SERVICE_CONTRACT = eINSTANCE.getServiceContract_HasServiceContract();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_CONTRACT__NAME = eINSTANCE.getServiceContract_Name();

		/**
		 * The meta object literal for the '<em><b>Has Candidate Services</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES = eINSTANCE.getServiceContract_HasCandidateServices();

		/**
		 * The meta object literal for the '<em><b>Candidate Services</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CONTRACT__CANDIDATE_SERVICES = eINSTANCE.getServiceContract_CandidateServices();

		/**
		 * The meta object literal for the '{@link SoaML.impl.CandidateServicesImpl <em>Candidate Services</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.CandidateServicesImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getCandidateServices()
		 * @generated
		 */
		EClass CANDIDATE_SERVICES = eINSTANCE.getCandidateServices();

		/**
		 * The meta object literal for the '<em><b>Has Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CANDIDATE_SERVICES__HAS_OPERATION = eINSTANCE.getCandidateServices_HasOperation();

		/**
		 * The meta object literal for the '{@link SoaML.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SoaML.impl.OperationImpl
		 * @see SoaML.impl.SoaMLPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__OPERATION = eINSTANCE.getOperation_Operation();

	}

} //SoaMLPackage
