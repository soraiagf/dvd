/**
 */
package SoaML.impl;

import SoaML.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SoaMLFactoryImpl extends EFactoryImpl implements SoaMLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SoaMLFactory init() {
		try {
			SoaMLFactory theSoaMLFactory = (SoaMLFactory)EPackage.Registry.INSTANCE.getEFactory(SoaMLPackage.eNS_URI);
			if (theSoaMLFactory != null) {
				return theSoaMLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SoaMLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoaMLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SoaMLPackage.SOA_ML: return createSoaML();
			case SoaMLPackage.SERVICES_ARCHITECTURE: return createServicesArchitecture();
			case SoaMLPackage.PARTICIPANT: return createParticipant();
			case SoaMLPackage.SERVICE_CONTRACT: return createServiceContract();
			case SoaMLPackage.CANDIDATE_SERVICES: return createCandidateServices();
			case SoaMLPackage.OPERATION: return createOperation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoaML createSoaML() {
		SoaMLImpl soaML = new SoaMLImpl();
		return soaML;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServicesArchitecture createServicesArchitecture() {
		ServicesArchitectureImpl servicesArchitecture = new ServicesArchitectureImpl();
		return servicesArchitecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant createParticipant() {
		ParticipantImpl participant = new ParticipantImpl();
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceContract createServiceContract() {
		ServiceContractImpl serviceContract = new ServiceContractImpl();
		return serviceContract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CandidateServices createCandidateServices() {
		CandidateServicesImpl candidateServices = new CandidateServicesImpl();
		return candidateServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoaMLPackage getSoaMLPackage() {
		return (SoaMLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SoaMLPackage getPackage() {
		return SoaMLPackage.eINSTANCE;
	}

} //SoaMLFactoryImpl
