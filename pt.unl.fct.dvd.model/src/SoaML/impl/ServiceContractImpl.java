/**
 */
package SoaML.impl;

import SoaML.CandidateServices;
import SoaML.Participant;
import SoaML.ServiceContract;
import SoaML.SoaMLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Contract</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SoaML.impl.ServiceContractImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link SoaML.impl.ServiceContractImpl#getHasServiceContract <em>Has Service Contract</em>}</li>
 *   <li>{@link SoaML.impl.ServiceContractImpl#getName <em>Name</em>}</li>
 *   <li>{@link SoaML.impl.ServiceContractImpl#getHasCandidateServices <em>Has Candidate Services</em>}</li>
 *   <li>{@link SoaML.impl.ServiceContractImpl#getCandidateServices <em>Candidate Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceContractImpl extends MinimalEObjectImpl.Container implements ServiceContract {
	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected Participant participant;

	/**
	 * The cached value of the '{@link #getHasServiceContract() <em>Has Service Contract</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasServiceContract()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceContract> hasServiceContract;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasCandidateServices() <em>Has Candidate Services</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasCandidateServices()
	 * @generated
	 * @ordered
	 */
	protected EList<CandidateServices> hasCandidateServices;

	/**
	 * The cached value of the '{@link #getCandidateServices() <em>Candidate Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCandidateServices()
	 * @generated
	 * @ordered
	 */
	protected EList<CandidateServices> candidateServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceContractImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SoaMLPackage.Literals.SERVICE_CONTRACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant getParticipant() {
		if (participant != null && participant.eIsProxy()) {
			InternalEObject oldParticipant = (InternalEObject)participant;
			participant = (Participant)eResolveProxy(oldParticipant);
			if (participant != oldParticipant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT, oldParticipant, participant));
			}
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant basicGetParticipant() {
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParticipant(Participant newParticipant) {
		Participant oldParticipant = participant;
		participant = newParticipant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT, oldParticipant, participant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceContract> getHasServiceContract() {
		if (hasServiceContract == null) {
			hasServiceContract = new EObjectResolvingEList<ServiceContract>(ServiceContract.class, this, SoaMLPackage.SERVICE_CONTRACT__HAS_SERVICE_CONTRACT);
		}
		return hasServiceContract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SoaMLPackage.SERVICE_CONTRACT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CandidateServices> getHasCandidateServices() {
		if (hasCandidateServices == null) {
			hasCandidateServices = new EObjectContainmentEList<CandidateServices>(CandidateServices.class, this, SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES);
		}
		return hasCandidateServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CandidateServices> getCandidateServices() {
		if (candidateServices == null) {
			candidateServices = new EObjectResolvingEList<CandidateServices>(CandidateServices.class, this, SoaMLPackage.SERVICE_CONTRACT__CANDIDATE_SERVICES);
		}
		return candidateServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES:
				return ((InternalEList<?>)getHasCandidateServices()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT:
				if (resolve) return getParticipant();
				return basicGetParticipant();
			case SoaMLPackage.SERVICE_CONTRACT__HAS_SERVICE_CONTRACT:
				return getHasServiceContract();
			case SoaMLPackage.SERVICE_CONTRACT__NAME:
				return getName();
			case SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES:
				return getHasCandidateServices();
			case SoaMLPackage.SERVICE_CONTRACT__CANDIDATE_SERVICES:
				return getCandidateServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT:
				setParticipant((Participant)newValue);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__HAS_SERVICE_CONTRACT:
				getHasServiceContract().clear();
				getHasServiceContract().addAll((Collection<? extends ServiceContract>)newValue);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__NAME:
				setName((String)newValue);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES:
				getHasCandidateServices().clear();
				getHasCandidateServices().addAll((Collection<? extends CandidateServices>)newValue);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__CANDIDATE_SERVICES:
				getCandidateServices().clear();
				getCandidateServices().addAll((Collection<? extends CandidateServices>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT:
				setParticipant((Participant)null);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__HAS_SERVICE_CONTRACT:
				getHasServiceContract().clear();
				return;
			case SoaMLPackage.SERVICE_CONTRACT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES:
				getHasCandidateServices().clear();
				return;
			case SoaMLPackage.SERVICE_CONTRACT__CANDIDATE_SERVICES:
				getCandidateServices().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SERVICE_CONTRACT__PARTICIPANT:
				return participant != null;
			case SoaMLPackage.SERVICE_CONTRACT__HAS_SERVICE_CONTRACT:
				return hasServiceContract != null && !hasServiceContract.isEmpty();
			case SoaMLPackage.SERVICE_CONTRACT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SoaMLPackage.SERVICE_CONTRACT__HAS_CANDIDATE_SERVICES:
				return hasCandidateServices != null && !hasCandidateServices.isEmpty();
			case SoaMLPackage.SERVICE_CONTRACT__CANDIDATE_SERVICES:
				return candidateServices != null && !candidateServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ServiceContractImpl
