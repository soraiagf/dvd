/**
 */
package SoaML.impl;

import SoaML.Participant;
import SoaML.ServiceContract;
import SoaML.ServicesArchitecture;
import SoaML.SoaMLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Services Architecture</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SoaML.impl.ServicesArchitectureImpl#getServiceContract <em>Service Contract</em>}</li>
 *   <li>{@link SoaML.impl.ServicesArchitectureImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link SoaML.impl.ServicesArchitectureImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServicesArchitectureImpl extends MinimalEObjectImpl.Container implements ServicesArchitecture {
	/**
	 * The cached value of the '{@link #getServiceContract() <em>Service Contract</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceContract()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceContract> serviceContract;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected EList<Participant> participant;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServicesArchitectureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SoaMLPackage.Literals.SERVICES_ARCHITECTURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceContract> getServiceContract() {
		if (serviceContract == null) {
			serviceContract = new EObjectContainmentEList<ServiceContract>(ServiceContract.class, this, SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT);
		}
		return serviceContract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Participant> getParticipant() {
		if (participant == null) {
			participant = new EObjectContainmentEList<Participant>(Participant.class, this, SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT);
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SoaMLPackage.SERVICES_ARCHITECTURE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT:
				return ((InternalEList<?>)getServiceContract()).basicRemove(otherEnd, msgs);
			case SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT:
				return ((InternalEList<?>)getParticipant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT:
				return getServiceContract();
			case SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT:
				return getParticipant();
			case SoaMLPackage.SERVICES_ARCHITECTURE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT:
				getServiceContract().clear();
				getServiceContract().addAll((Collection<? extends ServiceContract>)newValue);
				return;
			case SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT:
				getParticipant().clear();
				getParticipant().addAll((Collection<? extends Participant>)newValue);
				return;
			case SoaMLPackage.SERVICES_ARCHITECTURE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT:
				getServiceContract().clear();
				return;
			case SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT:
				getParticipant().clear();
				return;
			case SoaMLPackage.SERVICES_ARCHITECTURE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SERVICES_ARCHITECTURE__SERVICE_CONTRACT:
				return serviceContract != null && !serviceContract.isEmpty();
			case SoaMLPackage.SERVICES_ARCHITECTURE__PARTICIPANT:
				return participant != null && !participant.isEmpty();
			case SoaMLPackage.SERVICES_ARCHITECTURE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ServicesArchitectureImpl
