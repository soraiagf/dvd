/**
 */
package SoaML.impl;

import SoaML.ServicesArchitecture;
import SoaML.SoaML;
import SoaML.SoaMLPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Soa ML</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SoaML.impl.SoaMLImpl#getServicesArchitecture <em>Services Architecture</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoaMLImpl extends MinimalEObjectImpl.Container implements SoaML {
	/**
	 * The cached value of the '{@link #getServicesArchitecture() <em>Services Architecture</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServicesArchitecture()
	 * @generated
	 * @ordered
	 */
	protected ServicesArchitecture servicesArchitecture;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoaMLImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SoaMLPackage.Literals.SOA_ML;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServicesArchitecture getServicesArchitecture() {
		return servicesArchitecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetServicesArchitecture(ServicesArchitecture newServicesArchitecture, NotificationChain msgs) {
		ServicesArchitecture oldServicesArchitecture = servicesArchitecture;
		servicesArchitecture = newServicesArchitecture;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE, oldServicesArchitecture, newServicesArchitecture);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServicesArchitecture(ServicesArchitecture newServicesArchitecture) {
		if (newServicesArchitecture != servicesArchitecture) {
			NotificationChain msgs = null;
			if (servicesArchitecture != null)
				msgs = ((InternalEObject)servicesArchitecture).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE, null, msgs);
			if (newServicesArchitecture != null)
				msgs = ((InternalEObject)newServicesArchitecture).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE, null, msgs);
			msgs = basicSetServicesArchitecture(newServicesArchitecture, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE, newServicesArchitecture, newServicesArchitecture));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE:
				return basicSetServicesArchitecture(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE:
				return getServicesArchitecture();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE:
				setServicesArchitecture((ServicesArchitecture)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE:
				setServicesArchitecture((ServicesArchitecture)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SoaMLPackage.SOA_ML__SERVICES_ARCHITECTURE:
				return servicesArchitecture != null;
		}
		return super.eIsSet(featureID);
	}

} //SoaMLImpl
