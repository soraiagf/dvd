/**
 */
package SoaML;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Soa ML</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SoaML.SoaML#getServicesArchitecture <em>Services Architecture</em>}</li>
 * </ul>
 *
 * @see SoaML.SoaMLPackage#getSoaML()
 * @model
 * @generated
 */
public interface SoaML extends EObject {
	/**
	 * Returns the value of the '<em><b>Services Architecture</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Services Architecture</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Services Architecture</em>' containment reference.
	 * @see #setServicesArchitecture(ServicesArchitecture)
	 * @see SoaML.SoaMLPackage#getSoaML_ServicesArchitecture()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ServicesArchitecture getServicesArchitecture();

	/**
	 * Sets the value of the '{@link SoaML.SoaML#getServicesArchitecture <em>Services Architecture</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Services Architecture</em>' containment reference.
	 * @see #getServicesArchitecture()
	 * @generated
	 */
	void setServicesArchitecture(ServicesArchitecture value);

} // SoaML
