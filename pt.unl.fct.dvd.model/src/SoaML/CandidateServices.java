/**
 */
package SoaML;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Candidate Services</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SoaML.CandidateServices#getHasOperation <em>Has Operation</em>}</li>
 * </ul>
 *
 * @see SoaML.SoaMLPackage#getCandidateServices()
 * @model
 * @generated
 */
public interface CandidateServices extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Operation</b></em>' containment reference list.
	 * The list contents are of type {@link SoaML.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Operation</em>' containment reference list.
	 * @see SoaML.SoaMLPackage#getCandidateServices_HasOperation()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getHasOperation();

} // CandidateServices
