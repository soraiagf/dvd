/**
 */
package SoaML;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SoaML.ServiceContract#getParticipant <em>Participant</em>}</li>
 *   <li>{@link SoaML.ServiceContract#getHasServiceContract <em>Has Service Contract</em>}</li>
 *   <li>{@link SoaML.ServiceContract#getName <em>Name</em>}</li>
 *   <li>{@link SoaML.ServiceContract#getHasCandidateServices <em>Has Candidate Services</em>}</li>
 *   <li>{@link SoaML.ServiceContract#getCandidateServices <em>Candidate Services</em>}</li>
 * </ul>
 *
 * @see SoaML.SoaMLPackage#getServiceContract()
 * @model
 * @generated
 */
public interface ServiceContract extends EObject {
	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference.
	 * @see #setParticipant(Participant)
	 * @see SoaML.SoaMLPackage#getServiceContract_Participant()
	 * @model
	 * @generated
	 */
	Participant getParticipant();

	/**
	 * Sets the value of the '{@link SoaML.ServiceContract#getParticipant <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant</em>' reference.
	 * @see #getParticipant()
	 * @generated
	 */
	void setParticipant(Participant value);

	/**
	 * Returns the value of the '<em><b>Has Service Contract</b></em>' reference list.
	 * The list contents are of type {@link SoaML.ServiceContract}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Service Contract</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Service Contract</em>' reference list.
	 * @see SoaML.SoaMLPackage#getServiceContract_HasServiceContract()
	 * @model
	 * @generated
	 */
	EList<ServiceContract> getHasServiceContract();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see SoaML.SoaMLPackage#getServiceContract_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link SoaML.ServiceContract#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has Candidate Services</b></em>' containment reference list.
	 * The list contents are of type {@link SoaML.CandidateServices}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Candidate Services</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Candidate Services</em>' containment reference list.
	 * @see SoaML.SoaMLPackage#getServiceContract_HasCandidateServices()
	 * @model containment="true"
	 * @generated
	 */
	EList<CandidateServices> getHasCandidateServices();

	/**
	 * Returns the value of the '<em><b>Candidate Services</b></em>' reference list.
	 * The list contents are of type {@link SoaML.CandidateServices}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Candidate Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Candidate Services</em>' reference list.
	 * @see SoaML.SoaMLPackage#getServiceContract_CandidateServices()
	 * @model
	 * @generated
	 */
	EList<CandidateServices> getCandidateServices();

} // ServiceContract
