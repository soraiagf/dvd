/**
 */
package SoaML;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Participant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SoaML.Participant#getServiceContract <em>Service Contract</em>}</li>
 *   <li>{@link SoaML.Participant#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see SoaML.SoaMLPackage#getParticipant()
 * @model
 * @generated
 */
public interface Participant extends EObject {
	/**
	 * Returns the value of the '<em><b>Service Contract</b></em>' reference list.
	 * The list contents are of type {@link SoaML.ServiceContract}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Contract</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Contract</em>' reference list.
	 * @see SoaML.SoaMLPackage#getParticipant_ServiceContract()
	 * @model required="true"
	 * @generated
	 */
	EList<ServiceContract> getServiceContract();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see SoaML.SoaMLPackage#getParticipant_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link SoaML.Participant#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Participant
