/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceFactory
 * @model kind="package"
 * @generated
 */
public interface KAOS4ServicePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "KAOS4Service";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "pt.unl.fct.KAOS4Service";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "pt.unl.fct.KAOS4Service";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	KAOS4ServicePackage eINSTANCE = pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl.init();

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServiceModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServiceModelImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getKAOS4ServiceModel()
	 * @generated
	 */
	int KAOS4_SERVICE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KAOS4_SERVICE_MODEL__RELATIONS = 0;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KAOS4_SERVICE_MODEL__NODES = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KAOS4_SERVICE_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KAOS4_SERVICE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.NodeImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getNode()
	 * @generated
	 */
	int NODE = 1;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RelationImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 2;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.AgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.AgentImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getAgent()
	 * @generated
	 */
	int AGENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__NAME = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__OPERATION = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.GoalImpl <em>Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.GoalImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getGoal()
	 * @generated
	 */
	int GOAL = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__NAME = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__HAS_REFINEMENT = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl <em>Software Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getSoftwareAgent()
	 * @generated
	 */
	int SOFTWARE_AGENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__OPERATION = AGENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__SPECIALIZATION = AGENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__DVD_ELEMENT_ID = AGENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>RResponsability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__RRESPONSABILITY = AGENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Software Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT_FEATURE_COUNT = AGENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Software Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT_OPERATION_COUNT = AGENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl <em>Environment Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getEnvironmentAgent()
	 * @generated
	 */
	int ENVIRONMENT_AGENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__OPERATION = AGENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__SPECIALIZATION = AGENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EResponsability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__ERESPONSABILITY = AGENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__DVD_ELEMENT_ID = AGENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Environment Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT_FEATURE_COUNT = AGENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Environment Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT_OPERATION_COUNT = AGENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.BehaviouralGoalImpl <em>Behavioural Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.BehaviouralGoalImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getBehaviouralGoal()
	 * @generated
	 */
	int BEHAVIOURAL_GOAL = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOURAL_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOURAL_GOAL__HAS_REFINEMENT = GOAL__HAS_REFINEMENT;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOURAL_GOAL__DVD_ELEMENT_ID = GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Behavioural Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOURAL_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Behavioural Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOURAL_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getSoftGoal()
	 * @generated
	 */
	int SOFT_GOAL = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__HAS_REFINEMENT = GOAL__HAS_REFINEMENT;

	/**
	 * The feature id for the '<em><b>To Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__TO_REQUIREMENT = GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__DVD_ELEMENT_ID = GOAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.LeafGoalImpl <em>Leaf Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.LeafGoalImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getLeafGoal()
	 * @generated
	 */
	int LEAF_GOAL = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__HAS_REFINEMENT = GOAL__HAS_REFINEMENT;

	/**
	 * The number of structural features of the '<em>Leaf Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Leaf Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl <em>Expectation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getExpectation()
	 * @generated
	 */
	int EXPECTATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__NAME = LEAF_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__HAS_REFINEMENT = LEAF_GOAL__HAS_REFINEMENT;

	/**
	 * The feature id for the '<em><b>Environment Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__ENVIRONMENT_AGENT = LEAF_GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__DVD_ELEMENT_ID = LEAF_GOAL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__VALUE_OBJECT = LEAF_GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Expectation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION_FEATURE_COUNT = LEAF_GOAL_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Expectation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION_OPERATION_COUNT = LEAF_GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl <em>Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRequirement()
	 * @generated
	 */
	int REQUIREMENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__NAME = LEAF_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Has Refinement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__HAS_REFINEMENT = LEAF_GOAL__HAS_REFINEMENT;

	/**
	 * The feature id for the '<em><b>Require SG</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__REQUIRE_SG = LEAF_GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__DVD_ELEMENT_ID = LEAF_GOAL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__VALUE_OBJECT = LEAF_GOAL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Software Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__SOFTWARE_AGENT = LEAF_GOAL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_FEATURE_COUNT = LEAF_GOAL_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_OPERATION_COUNT = LEAF_GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl <em>Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRefinement()
	 * @generated
	 */
	int REFINEMENT = 12;

	/**
	 * The feature id for the '<em><b>Logical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT__LOGICAL = RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT__HAS_GOAL = RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT__OPERATION = RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Has Logical</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT__HAS_LOGICAL = RELATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Logical O</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT__LOGICAL_O = RELATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_FEATURE_COUNT = RELATION_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 13;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OPERATION = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__AGENT = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ORefinement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OREFINEMENT = NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl <em>Operation Order</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getOperation_Order()
	 * @generated
	 */
	int OPERATION_ORDER = 14;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER__OPERATION = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER__AGENT = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ORefinement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER__OREFINEMENT = NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER__ORDER = NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER__SCENARIO = NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Operation Order</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER_FEATURE_COUNT = NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Operation Order</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ORDER_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ScenarioImpl <em>Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.ScenarioImpl
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getScenario()
	 * @generated
	 */
	int SCENARIO = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__ID = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__TITLE = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__DESCRIPTION = NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__COLOR = NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__OPERATION = NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_FEATURE_COUNT = NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator <em>Logical Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getLogicalOperator()
	 * @generated
	 */
	int LOGICAL_OPERATOR = 16;

	/**
	 * The meta object id for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Color <em>Color</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Color
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getColor()
	 * @generated
	 */
	int COLOR = 17;


	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel
	 * @generated
	 */
	EClass getKAOS4ServiceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel#getRelations <em>Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel#getRelations()
	 * @see #getKAOS4ServiceModel()
	 * @generated
	 */
	EReference getKAOS4ServiceModel_Relations();

	/**
	 * Returns the meta object for the containment reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServiceModel#getNodes()
	 * @see #getKAOS4ServiceModel()
	 * @generated
	 */
	EReference getKAOS4ServiceModel_Nodes();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Agent
	 * @generated
	 */
	EClass getAgent();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getName()
	 * @see #getAgent()
	 * @generated
	 */
	EAttribute getAgent_Name();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getOperation()
	 * @see #getAgent()
	 * @generated
	 */
	EReference getAgent_Operation();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Goal
	 * @generated
	 */
	EClass getGoal();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getName()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Name();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getHasRefinement <em>Has Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Refinement</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getHasRefinement()
	 * @see #getGoal()
	 * @generated
	 */
	EReference getGoal_HasRefinement();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent <em>Software Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent
	 * @generated
	 */
	EClass getSoftwareAgent();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Specialization</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getSpecialization()
	 * @see #getSoftwareAgent()
	 * @generated
	 */
	EReference getSoftwareAgent_Specialization();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getDVDElementID()
	 * @see #getSoftwareAgent()
	 * @generated
	 */
	EAttribute getSoftwareAgent_DVDElementID();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getRResponsability <em>RResponsability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>RResponsability</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getRResponsability()
	 * @see #getSoftwareAgent()
	 * @generated
	 */
	EReference getSoftwareAgent_RResponsability();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent <em>Environment Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent
	 * @generated
	 */
	EClass getEnvironmentAgent();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Specialization</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getSpecialization()
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	EReference getEnvironmentAgent_Specialization();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getEResponsability <em>EResponsability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>EResponsability</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getEResponsability()
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	EReference getEnvironmentAgent_EResponsability();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getDVDElementID()
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	EAttribute getEnvironmentAgent_DVDElementID();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal <em>Behavioural Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavioural Goal</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal
	 * @generated
	 */
	EClass getBehaviouralGoal();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal#getDVDElementID()
	 * @see #getBehaviouralGoal()
	 * @generated
	 */
	EAttribute getBehaviouralGoal_DVDElementID();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal <em>Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Goal</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal
	 * @generated
	 */
	EClass getSoftGoal();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement <em>To Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Requirement</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EReference getSoftGoal_ToRequirement();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getDVDElementID()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_DVDElementID();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.LeafGoal <em>Leaf Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Goal</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LeafGoal
	 * @generated
	 */
	EClass getLeafGoal();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation <em>Expectation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expectation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Expectation
	 * @generated
	 */
	EClass getExpectation();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent <em>Environment Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent()
	 * @see #getExpectation()
	 * @generated
	 */
	EReference getExpectation_EnvironmentAgent();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getDVDElementID()
	 * @see #getExpectation()
	 * @generated
	 */
	EAttribute getExpectation_DVDElementID();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getValueObject <em>Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Object</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getValueObject()
	 * @see #getExpectation()
	 * @generated
	 */
	EAttribute getExpectation_ValueObject();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement
	 * @generated
	 */
	EClass getRequirement();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getRequireSG <em>Require SG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Require SG</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getRequireSG()
	 * @see #getRequirement()
	 * @generated
	 */
	EReference getRequirement_RequireSG();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getDVDElementID <em>DVD Element ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DVD Element ID</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getDVDElementID()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_DVDElementID();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getValueObject <em>Value Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Object</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getValueObject()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_ValueObject();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent <em>Software Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Software Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent()
	 * @see #getRequirement()
	 * @generated
	 */
	EReference getRequirement_SoftwareAgent();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement <em>Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Refinement</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement
	 * @generated
	 */
	EClass getRefinement();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogical <em>Logical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Logical</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogical()
	 * @see #getRefinement()
	 * @generated
	 */
	EAttribute getRefinement_Logical();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasGoal <em>Has Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Has Goal</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasGoal()
	 * @see #getRefinement()
	 * @generated
	 */
	EReference getRefinement_HasGoal();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getOperation()
	 * @see #getRefinement()
	 * @generated
	 */
	EReference getRefinement_Operation();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical <em>Has Logical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Has Logical</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical()
	 * @see #getRefinement()
	 * @generated
	 */
	EReference getRefinement_HasLogical();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogicalO <em>Logical O</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Logical O</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogicalO()
	 * @see #getRefinement()
	 * @generated
	 */
	EReference getRefinement_LogicalO();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getOperation()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Operation();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Agent</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getAgent()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Agent();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getORefinement <em>ORefinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ORefinement</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getORefinement()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_ORefinement();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order <em>Operation Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Order</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order
	 * @generated
	 */
	EClass getOperation_Order();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order#getOrder <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order#getOrder()
	 * @see #getOperation_Order()
	 * @generated
	 */
	EAttribute getOperation_Order_Order();

	/**
	 * Returns the meta object for the reference '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order#getScenario()
	 * @see #getOperation_Order()
	 * @generated
	 */
	EReference getOperation_Order_Scenario();

	/**
	 * Returns the meta object for class '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario
	 * @generated
	 */
	EClass getScenario();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getId()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Id();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getTitle()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Title();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getDescription()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Description();

	/**
	 * Returns the meta object for the attribute '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getColor()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Color();

	/**
	 * Returns the meta object for the reference list '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operation</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Scenario#getOperation()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_Operation();

	/**
	 * Returns the meta object for enum '{@link pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator
	 * @generated
	 */
	EEnum getLogicalOperator();

	/**
	 * Returns the meta object for enum '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Color</em>'.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Color
	 * @generated
	 */
	EEnum getColor();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	KAOS4ServiceFactory getKAOS4ServiceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServiceModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServiceModelImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getKAOS4ServiceModel()
		 * @generated
		 */
		EClass KAOS4_SERVICE_MODEL = eINSTANCE.getKAOS4ServiceModel();

		/**
		 * The meta object literal for the '<em><b>Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KAOS4_SERVICE_MODEL__RELATIONS = eINSTANCE.getKAOS4ServiceModel_Relations();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KAOS4_SERVICE_MODEL__NODES = eINSTANCE.getKAOS4ServiceModel_Nodes();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.NodeImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RelationImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.AgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.AgentImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getAgent()
		 * @generated
		 */
		EClass AGENT = eINSTANCE.getAgent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT__NAME = eINSTANCE.getAgent_Name();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT__OPERATION = eINSTANCE.getAgent_Operation();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.GoalImpl <em>Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.GoalImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getGoal()
		 * @generated
		 */
		EClass GOAL = eINSTANCE.getGoal();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__NAME = eINSTANCE.getGoal_Name();

		/**
		 * The meta object literal for the '<em><b>Has Refinement</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL__HAS_REFINEMENT = eINSTANCE.getGoal_HasRefinement();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl <em>Software Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getSoftwareAgent()
		 * @generated
		 */
		EClass SOFTWARE_AGENT = eINSTANCE.getSoftwareAgent();

		/**
		 * The meta object literal for the '<em><b>Specialization</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_AGENT__SPECIALIZATION = eINSTANCE.getSoftwareAgent_Specialization();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFTWARE_AGENT__DVD_ELEMENT_ID = eINSTANCE.getSoftwareAgent_DVDElementID();

		/**
		 * The meta object literal for the '<em><b>RResponsability</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_AGENT__RRESPONSABILITY = eINSTANCE.getSoftwareAgent_RResponsability();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl <em>Environment Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getEnvironmentAgent()
		 * @generated
		 */
		EClass ENVIRONMENT_AGENT = eINSTANCE.getEnvironmentAgent();

		/**
		 * The meta object literal for the '<em><b>Specialization</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_AGENT__SPECIALIZATION = eINSTANCE.getEnvironmentAgent_Specialization();

		/**
		 * The meta object literal for the '<em><b>EResponsability</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_AGENT__ERESPONSABILITY = eINSTANCE.getEnvironmentAgent_EResponsability();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT_AGENT__DVD_ELEMENT_ID = eINSTANCE.getEnvironmentAgent_DVDElementID();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.BehaviouralGoalImpl <em>Behavioural Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.BehaviouralGoalImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getBehaviouralGoal()
		 * @generated
		 */
		EClass BEHAVIOURAL_GOAL = eINSTANCE.getBehaviouralGoal();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEHAVIOURAL_GOAL__DVD_ELEMENT_ID = eINSTANCE.getBehaviouralGoal_DVDElementID();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getSoftGoal()
		 * @generated
		 */
		EClass SOFT_GOAL = eINSTANCE.getSoftGoal();

		/**
		 * The meta object literal for the '<em><b>To Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFT_GOAL__TO_REQUIREMENT = eINSTANCE.getSoftGoal_ToRequirement();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__DVD_ELEMENT_ID = eINSTANCE.getSoftGoal_DVDElementID();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.LeafGoalImpl <em>Leaf Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.LeafGoalImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getLeafGoal()
		 * @generated
		 */
		EClass LEAF_GOAL = eINSTANCE.getLeafGoal();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl <em>Expectation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getExpectation()
		 * @generated
		 */
		EClass EXPECTATION = eINSTANCE.getExpectation();

		/**
		 * The meta object literal for the '<em><b>Environment Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPECTATION__ENVIRONMENT_AGENT = eINSTANCE.getExpectation_EnvironmentAgent();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPECTATION__DVD_ELEMENT_ID = eINSTANCE.getExpectation_DVDElementID();

		/**
		 * The meta object literal for the '<em><b>Value Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPECTATION__VALUE_OBJECT = eINSTANCE.getExpectation_ValueObject();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl <em>Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRequirement()
		 * @generated
		 */
		EClass REQUIREMENT = eINSTANCE.getRequirement();

		/**
		 * The meta object literal for the '<em><b>Require SG</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT__REQUIRE_SG = eINSTANCE.getRequirement_RequireSG();

		/**
		 * The meta object literal for the '<em><b>DVD Element ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__DVD_ELEMENT_ID = eINSTANCE.getRequirement_DVDElementID();

		/**
		 * The meta object literal for the '<em><b>Value Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__VALUE_OBJECT = eINSTANCE.getRequirement_ValueObject();

		/**
		 * The meta object literal for the '<em><b>Software Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT__SOFTWARE_AGENT = eINSTANCE.getRequirement_SoftwareAgent();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl <em>Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getRefinement()
		 * @generated
		 */
		EClass REFINEMENT = eINSTANCE.getRefinement();

		/**
		 * The meta object literal for the '<em><b>Logical</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFINEMENT__LOGICAL = eINSTANCE.getRefinement_Logical();

		/**
		 * The meta object literal for the '<em><b>Has Goal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFINEMENT__HAS_GOAL = eINSTANCE.getRefinement_HasGoal();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFINEMENT__OPERATION = eINSTANCE.getRefinement_Operation();

		/**
		 * The meta object literal for the '<em><b>Has Logical</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFINEMENT__HAS_LOGICAL = eINSTANCE.getRefinement_HasLogical();

		/**
		 * The meta object literal for the '<em><b>Logical O</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFINEMENT__LOGICAL_O = eINSTANCE.getRefinement_LogicalO();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__OPERATION = eINSTANCE.getOperation_Operation();

		/**
		 * The meta object literal for the '<em><b>Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__AGENT = eINSTANCE.getOperation_Agent();

		/**
		 * The meta object literal for the '<em><b>ORefinement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__OREFINEMENT = eINSTANCE.getOperation_ORefinement();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl <em>Operation Order</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getOperation_Order()
		 * @generated
		 */
		EClass OPERATION_ORDER = eINSTANCE.getOperation_Order();

		/**
		 * The meta object literal for the '<em><b>Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_ORDER__ORDER = eINSTANCE.getOperation_Order_Order();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_ORDER__SCENARIO = eINSTANCE.getOperation_Order_Scenario();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ScenarioImpl <em>Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.ScenarioImpl
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getScenario()
		 * @generated
		 */
		EClass SCENARIO = eINSTANCE.getScenario();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__ID = eINSTANCE.getScenario_Id();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__TITLE = eINSTANCE.getScenario_Title();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__DESCRIPTION = eINSTANCE.getScenario_Description();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__COLOR = eINSTANCE.getScenario_Color();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__OPERATION = eINSTANCE.getScenario_Operation();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator <em>Logical Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getLogicalOperator()
		 * @generated
		 */
		EEnum LOGICAL_OPERATOR = eINSTANCE.getLogicalOperator();

		/**
		 * The meta object literal for the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Color <em>Color</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Color
		 * @see pt.unl.fct.KAOS4Service.KAOS4Service.impl.KAOS4ServicePackageImpl#getColor()
		 * @generated
		 */
		EEnum COLOR = eINSTANCE.getColor();

	}

} //KAOS4ServicePackage
