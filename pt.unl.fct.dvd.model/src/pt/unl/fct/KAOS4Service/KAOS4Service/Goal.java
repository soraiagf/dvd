/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getName <em>Name</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getHasRefinement <em>Has Refinement</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getGoal()
 * @model abstract="true"
 * @generated
 */
public interface Goal extends Node {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getGoal_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Goal#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has Refinement</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Refinement</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Refinement</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getGoal_HasRefinement()
	 * @model
	 * @generated
	 */
	EList<Refinement> getHasRefinement();

} // Goal
