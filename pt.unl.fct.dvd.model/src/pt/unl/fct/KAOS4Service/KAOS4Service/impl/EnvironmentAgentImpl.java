/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent;
import pt.unl.fct.KAOS4Service.KAOS4Service.Expectation;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl#getEResponsability <em>EResponsability</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.EnvironmentAgentImpl#getDVDElementID <em>DVD Element ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentAgentImpl extends AgentImpl implements EnvironmentAgent {
	/**
	 * The cached value of the '{@link #getSpecialization() <em>Specialization</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected EList<EnvironmentAgent> specialization;

	/**
	 * The cached value of the '{@link #getEResponsability() <em>EResponsability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEResponsability()
	 * @generated
	 * @ordered
	 */
	protected EList<Expectation> eResponsability;

	/**
	 * The default value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int DVD_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected int dvdElementID = DVD_ELEMENT_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.ENVIRONMENT_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnvironmentAgent> getSpecialization() {
		if (specialization == null) {
			specialization = new EObjectResolvingEList<EnvironmentAgent>(EnvironmentAgent.class, this, KAOS4ServicePackage.ENVIRONMENT_AGENT__SPECIALIZATION);
		}
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expectation> getEResponsability() {
		if (eResponsability == null) {
			eResponsability = new EObjectWithInverseResolvingEList<Expectation>(Expectation.class, this, KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY, KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT);
		}
		return eResponsability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDVDElementID() {
		return dvdElementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDVDElementID(int newDVDElementID) {
		int oldDVDElementID = dvdElementID;
		dvdElementID = newDVDElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.ENVIRONMENT_AGENT__DVD_ELEMENT_ID, oldDVDElementID, dvdElementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEResponsability()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				return ((InternalEList<?>)getEResponsability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__SPECIALIZATION:
				return getSpecialization();
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				return getEResponsability();
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__DVD_ELEMENT_ID:
				return getDVDElementID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__SPECIALIZATION:
				getSpecialization().clear();
				getSpecialization().addAll((Collection<? extends EnvironmentAgent>)newValue);
				return;
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				getEResponsability().clear();
				getEResponsability().addAll((Collection<? extends Expectation>)newValue);
				return;
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__DVD_ELEMENT_ID:
				setDVDElementID((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__SPECIALIZATION:
				getSpecialization().clear();
				return;
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				getEResponsability().clear();
				return;
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__DVD_ELEMENT_ID:
				setDVDElementID(DVD_ELEMENT_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__SPECIALIZATION:
				return specialization != null && !specialization.isEmpty();
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY:
				return eResponsability != null && !eResponsability.isEmpty();
			case KAOS4ServicePackage.ENVIRONMENT_AGENT__DVD_ELEMENT_ID:
				return dvdElementID != DVD_ELEMENT_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DVDElementID: ");
		result.append(dvdElementID);
		result.append(')');
		return result.toString();
	}

} //EnvironmentAgentImpl
