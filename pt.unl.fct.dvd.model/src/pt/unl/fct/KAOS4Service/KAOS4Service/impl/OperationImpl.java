/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.KAOS4Service.KAOS4Service.Agent;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.Operation;
import pt.unl.fct.KAOS4Service.KAOS4Service.Refinement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl#getAgent <em>Agent</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.OperationImpl#getORefinement <em>ORefinement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationImpl extends NodeImpl implements Operation {
	/**
	 * The default value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected String operation = OPERATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAgent() <em>Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgent()
	 * @generated
	 * @ordered
	 */
	protected Agent agent;

	/**
	 * The cached value of the '{@link #getORefinement() <em>ORefinement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getORefinement()
	 * @generated
	 * @ordered
	 */
	protected Refinement oRefinement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(String newOperation) {
		String oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getAgent() {
		if (agent != null && agent.eIsProxy()) {
			InternalEObject oldAgent = (InternalEObject)agent;
			agent = (Agent)eResolveProxy(oldAgent);
			if (agent != oldAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.OPERATION__AGENT, oldAgent, agent));
			}
		}
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent basicGetAgent() {
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAgent(Agent newAgent, NotificationChain msgs) {
		Agent oldAgent = agent;
		agent = newAgent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION__AGENT, oldAgent, newAgent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgent(Agent newAgent) {
		if (newAgent != agent) {
			NotificationChain msgs = null;
			if (agent != null)
				msgs = ((InternalEObject)agent).eInverseRemove(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
			if (newAgent != null)
				msgs = ((InternalEObject)newAgent).eInverseAdd(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
			msgs = basicSetAgent(newAgent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION__AGENT, newAgent, newAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement getORefinement() {
		if (oRefinement != null && oRefinement.eIsProxy()) {
			InternalEObject oldORefinement = (InternalEObject)oRefinement;
			oRefinement = (Refinement)eResolveProxy(oldORefinement);
			if (oRefinement != oldORefinement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.OPERATION__OREFINEMENT, oldORefinement, oRefinement));
			}
		}
		return oRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement basicGetORefinement() {
		return oRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetORefinement(Refinement newORefinement, NotificationChain msgs) {
		Refinement oldORefinement = oRefinement;
		oRefinement = newORefinement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION__OREFINEMENT, oldORefinement, newORefinement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setORefinement(Refinement newORefinement) {
		if (newORefinement != oRefinement) {
			NotificationChain msgs = null;
			if (oRefinement != null)
				msgs = ((InternalEObject)oRefinement).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
			if (newORefinement != null)
				msgs = ((InternalEObject)newORefinement).eInverseAdd(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
			msgs = basicSetORefinement(newORefinement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION__OREFINEMENT, newORefinement, newORefinement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__AGENT:
				if (agent != null)
					msgs = ((InternalEObject)agent).eInverseRemove(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
				return basicSetAgent((Agent)otherEnd, msgs);
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				if (oRefinement != null)
					msgs = ((InternalEObject)oRefinement).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
				return basicSetORefinement((Refinement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__AGENT:
				return basicSetAgent(null, msgs);
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				return basicSetORefinement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__OPERATION:
				return getOperation();
			case KAOS4ServicePackage.OPERATION__AGENT:
				if (resolve) return getAgent();
				return basicGetAgent();
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				if (resolve) return getORefinement();
				return basicGetORefinement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__OPERATION:
				setOperation((String)newValue);
				return;
			case KAOS4ServicePackage.OPERATION__AGENT:
				setAgent((Agent)newValue);
				return;
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				setORefinement((Refinement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__OPERATION:
				setOperation(OPERATION_EDEFAULT);
				return;
			case KAOS4ServicePackage.OPERATION__AGENT:
				setAgent((Agent)null);
				return;
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				setORefinement((Refinement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION__OPERATION:
				return OPERATION_EDEFAULT == null ? operation != null : !OPERATION_EDEFAULT.equals(operation);
			case KAOS4ServicePackage.OPERATION__AGENT:
				return agent != null;
			case KAOS4ServicePackage.OPERATION__OREFINEMENT:
				return oRefinement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operation: ");
		result.append(operation);
		result.append(')');
		return result.toString();
	}

} //OperationImpl
