/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.KAOS4Service.KAOS4Service.Goal;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator;
import pt.unl.fct.KAOS4Service.KAOS4Service.Operation;
import pt.unl.fct.KAOS4Service.KAOS4Service.Refinement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl#getLogical <em>Logical</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl#getHasGoal <em>Has Goal</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl#getHasLogical <em>Has Logical</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RefinementImpl#getLogicalO <em>Logical O</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RefinementImpl extends RelationImpl implements Refinement {
	/**
	 * The default value of the '{@link #getLogical() <em>Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogical()
	 * @generated
	 * @ordered
	 */
	protected static final LogicalOperator LOGICAL_EDEFAULT = LogicalOperator.AND;

	/**
	 * The cached value of the '{@link #getLogical() <em>Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogical()
	 * @generated
	 * @ordered
	 */
	protected LogicalOperator logical = LOGICAL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasGoal() <em>Has Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasGoal()
	 * @generated
	 * @ordered
	 */
	protected Goal hasGoal;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operation;

	/**
	 * The cached value of the '{@link #getHasLogical() <em>Has Logical</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLogical()
	 * @generated
	 * @ordered
	 */
	protected Refinement hasLogical;

	/**
	 * The cached value of the '{@link #getLogicalO() <em>Logical O</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalO()
	 * @generated
	 * @ordered
	 */
	protected EList<Refinement> logicalO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperator getLogical() {
		return logical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogical(LogicalOperator newLogical) {
		LogicalOperator oldLogical = logical;
		logical = newLogical == null ? LOGICAL_EDEFAULT : newLogical;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REFINEMENT__LOGICAL, oldLogical, logical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal getHasGoal() {
		if (hasGoal != null && hasGoal.eIsProxy()) {
			InternalEObject oldHasGoal = (InternalEObject)hasGoal;
			hasGoal = (Goal)eResolveProxy(oldHasGoal);
			if (hasGoal != oldHasGoal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.REFINEMENT__HAS_GOAL, oldHasGoal, hasGoal));
			}
		}
		return hasGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal basicGetHasGoal() {
		return hasGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasGoal(Goal newHasGoal) {
		Goal oldHasGoal = hasGoal;
		hasGoal = newHasGoal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REFINEMENT__HAS_GOAL, oldHasGoal, hasGoal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperation() {
		if (operation == null) {
			operation = new EObjectWithInverseResolvingEList<Operation>(Operation.class, this, KAOS4ServicePackage.REFINEMENT__OPERATION, KAOS4ServicePackage.OPERATION__OREFINEMENT);
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement getHasLogical() {
		if (hasLogical != null && hasLogical.eIsProxy()) {
			InternalEObject oldHasLogical = (InternalEObject)hasLogical;
			hasLogical = (Refinement)eResolveProxy(oldHasLogical);
			if (hasLogical != oldHasLogical) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL, oldHasLogical, hasLogical));
			}
		}
		return hasLogical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement basicGetHasLogical() {
		return hasLogical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLogical(Refinement newHasLogical, NotificationChain msgs) {
		Refinement oldHasLogical = hasLogical;
		hasLogical = newHasLogical;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL, oldHasLogical, newHasLogical);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLogical(Refinement newHasLogical) {
		if (newHasLogical != hasLogical) {
			NotificationChain msgs = null;
			if (hasLogical != null)
				msgs = ((InternalEObject)hasLogical).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__LOGICAL_O, Refinement.class, msgs);
			if (newHasLogical != null)
				msgs = ((InternalEObject)newHasLogical).eInverseAdd(this, KAOS4ServicePackage.REFINEMENT__LOGICAL_O, Refinement.class, msgs);
			msgs = basicSetHasLogical(newHasLogical, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL, newHasLogical, newHasLogical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Refinement> getLogicalO() {
		if (logicalO == null) {
			logicalO = new EObjectWithInverseResolvingEList<Refinement>(Refinement.class, this, KAOS4ServicePackage.REFINEMENT__LOGICAL_O, KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL);
		}
		return logicalO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOperation()).basicAdd(otherEnd, msgs);
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				if (hasLogical != null)
					msgs = ((InternalEObject)hasLogical).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__LOGICAL_O, Refinement.class, msgs);
				return basicSetHasLogical((Refinement)otherEnd, msgs);
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLogicalO()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				return ((InternalEList<?>)getOperation()).basicRemove(otherEnd, msgs);
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				return basicSetHasLogical(null, msgs);
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				return ((InternalEList<?>)getLogicalO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__LOGICAL:
				return getLogical();
			case KAOS4ServicePackage.REFINEMENT__HAS_GOAL:
				if (resolve) return getHasGoal();
				return basicGetHasGoal();
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				return getOperation();
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				if (resolve) return getHasLogical();
				return basicGetHasLogical();
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				return getLogicalO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__LOGICAL:
				setLogical((LogicalOperator)newValue);
				return;
			case KAOS4ServicePackage.REFINEMENT__HAS_GOAL:
				setHasGoal((Goal)newValue);
				return;
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				getOperation().clear();
				getOperation().addAll((Collection<? extends Operation>)newValue);
				return;
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				setHasLogical((Refinement)newValue);
				return;
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				getLogicalO().clear();
				getLogicalO().addAll((Collection<? extends Refinement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__LOGICAL:
				setLogical(LOGICAL_EDEFAULT);
				return;
			case KAOS4ServicePackage.REFINEMENT__HAS_GOAL:
				setHasGoal((Goal)null);
				return;
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				getOperation().clear();
				return;
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				setHasLogical((Refinement)null);
				return;
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				getLogicalO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.REFINEMENT__LOGICAL:
				return logical != LOGICAL_EDEFAULT;
			case KAOS4ServicePackage.REFINEMENT__HAS_GOAL:
				return hasGoal != null;
			case KAOS4ServicePackage.REFINEMENT__OPERATION:
				return operation != null && !operation.isEmpty();
			case KAOS4ServicePackage.REFINEMENT__HAS_LOGICAL:
				return hasLogical != null;
			case KAOS4ServicePackage.REFINEMENT__LOGICAL_O:
				return logicalO != null && !logicalO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (logical: ");
		result.append(logical);
		result.append(')');
		return result.toString();
	}

} //RefinementImpl
