/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.Requirement;
import pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftwareAgentImpl#getRResponsability <em>RResponsability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareAgentImpl extends AgentImpl implements SoftwareAgent {
	/**
	 * The cached value of the '{@link #getSpecialization() <em>Specialization</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected EList<SoftwareAgent> specialization;

	/**
	 * The default value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int DVD_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected int dvdElementID = DVD_ELEMENT_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRResponsability() <em>RResponsability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRResponsability()
	 * @generated
	 * @ordered
	 */
	protected EList<Requirement> rResponsability;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.SOFTWARE_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SoftwareAgent> getSpecialization() {
		if (specialization == null) {
			specialization = new EObjectResolvingEList<SoftwareAgent>(SoftwareAgent.class, this, KAOS4ServicePackage.SOFTWARE_AGENT__SPECIALIZATION);
		}
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDVDElementID() {
		return dvdElementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDVDElementID(int newDVDElementID) {
		int oldDVDElementID = dvdElementID;
		dvdElementID = newDVDElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.SOFTWARE_AGENT__DVD_ELEMENT_ID, oldDVDElementID, dvdElementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Requirement> getRResponsability() {
		if (rResponsability == null) {
			rResponsability = new EObjectWithInverseResolvingEList<Requirement>(Requirement.class, this, KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY, KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT);
		}
		return rResponsability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRResponsability()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				return ((InternalEList<?>)getRResponsability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__SPECIALIZATION:
				return getSpecialization();
			case KAOS4ServicePackage.SOFTWARE_AGENT__DVD_ELEMENT_ID:
				return getDVDElementID();
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				return getRResponsability();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__SPECIALIZATION:
				getSpecialization().clear();
				getSpecialization().addAll((Collection<? extends SoftwareAgent>)newValue);
				return;
			case KAOS4ServicePackage.SOFTWARE_AGENT__DVD_ELEMENT_ID:
				setDVDElementID((Integer)newValue);
				return;
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				getRResponsability().clear();
				getRResponsability().addAll((Collection<? extends Requirement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__SPECIALIZATION:
				getSpecialization().clear();
				return;
			case KAOS4ServicePackage.SOFTWARE_AGENT__DVD_ELEMENT_ID:
				setDVDElementID(DVD_ELEMENT_ID_EDEFAULT);
				return;
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				getRResponsability().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFTWARE_AGENT__SPECIALIZATION:
				return specialization != null && !specialization.isEmpty();
			case KAOS4ServicePackage.SOFTWARE_AGENT__DVD_ELEMENT_ID:
				return dvdElementID != DVD_ELEMENT_ID_EDEFAULT;
			case KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY:
				return rResponsability != null && !rResponsability.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DVDElementID: ");
		result.append(dvdElementID);
		result.append(')');
		return result.toString();
	}

} //SoftwareAgentImpl
