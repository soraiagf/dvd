/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent;
import pt.unl.fct.KAOS4Service.KAOS4Service.Expectation;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expectation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl#getEnvironmentAgent <em>Environment Agent</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.ExpectationImpl#getValueObject <em>Value Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpectationImpl extends LeafGoalImpl implements Expectation {
	/**
	 * The cached value of the '{@link #getEnvironmentAgent() <em>Environment Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironmentAgent()
	 * @generated
	 * @ordered
	 */
	protected EnvironmentAgent environmentAgent;

	/**
	 * The default value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int DVD_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected int dvdElementID = DVD_ELEMENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueObject() <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueObject()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_OBJECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueObject() <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueObject()
	 * @generated
	 * @ordered
	 */
	protected String valueObject = VALUE_OBJECT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpectationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.EXPECTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgent getEnvironmentAgent() {
		if (environmentAgent != null && environmentAgent.eIsProxy()) {
			InternalEObject oldEnvironmentAgent = (InternalEObject)environmentAgent;
			environmentAgent = (EnvironmentAgent)eResolveProxy(oldEnvironmentAgent);
			if (environmentAgent != oldEnvironmentAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT, oldEnvironmentAgent, environmentAgent));
			}
		}
		return environmentAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgent basicGetEnvironmentAgent() {
		return environmentAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironmentAgent(EnvironmentAgent newEnvironmentAgent, NotificationChain msgs) {
		EnvironmentAgent oldEnvironmentAgent = environmentAgent;
		environmentAgent = newEnvironmentAgent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT, oldEnvironmentAgent, newEnvironmentAgent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironmentAgent(EnvironmentAgent newEnvironmentAgent) {
		if (newEnvironmentAgent != environmentAgent) {
			NotificationChain msgs = null;
			if (environmentAgent != null)
				msgs = ((InternalEObject)environmentAgent).eInverseRemove(this, KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY, EnvironmentAgent.class, msgs);
			if (newEnvironmentAgent != null)
				msgs = ((InternalEObject)newEnvironmentAgent).eInverseAdd(this, KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY, EnvironmentAgent.class, msgs);
			msgs = basicSetEnvironmentAgent(newEnvironmentAgent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT, newEnvironmentAgent, newEnvironmentAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDVDElementID() {
		return dvdElementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDVDElementID(int newDVDElementID) {
		int oldDVDElementID = dvdElementID;
		dvdElementID = newDVDElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.EXPECTATION__DVD_ELEMENT_ID, oldDVDElementID, dvdElementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueObject() {
		return valueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueObject(String newValueObject) {
		String oldValueObject = valueObject;
		valueObject = newValueObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.EXPECTATION__VALUE_OBJECT, oldValueObject, valueObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				if (environmentAgent != null)
					msgs = ((InternalEObject)environmentAgent).eInverseRemove(this, KAOS4ServicePackage.ENVIRONMENT_AGENT__ERESPONSABILITY, EnvironmentAgent.class, msgs);
				return basicSetEnvironmentAgent((EnvironmentAgent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				return basicSetEnvironmentAgent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				if (resolve) return getEnvironmentAgent();
				return basicGetEnvironmentAgent();
			case KAOS4ServicePackage.EXPECTATION__DVD_ELEMENT_ID:
				return getDVDElementID();
			case KAOS4ServicePackage.EXPECTATION__VALUE_OBJECT:
				return getValueObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				setEnvironmentAgent((EnvironmentAgent)newValue);
				return;
			case KAOS4ServicePackage.EXPECTATION__DVD_ELEMENT_ID:
				setDVDElementID((Integer)newValue);
				return;
			case KAOS4ServicePackage.EXPECTATION__VALUE_OBJECT:
				setValueObject((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				setEnvironmentAgent((EnvironmentAgent)null);
				return;
			case KAOS4ServicePackage.EXPECTATION__DVD_ELEMENT_ID:
				setDVDElementID(DVD_ELEMENT_ID_EDEFAULT);
				return;
			case KAOS4ServicePackage.EXPECTATION__VALUE_OBJECT:
				setValueObject(VALUE_OBJECT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.EXPECTATION__ENVIRONMENT_AGENT:
				return environmentAgent != null;
			case KAOS4ServicePackage.EXPECTATION__DVD_ELEMENT_ID:
				return dvdElementID != DVD_ELEMENT_ID_EDEFAULT;
			case KAOS4ServicePackage.EXPECTATION__VALUE_OBJECT:
				return VALUE_OBJECT_EDEFAULT == null ? valueObject != null : !VALUE_OBJECT_EDEFAULT.equals(valueObject);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DVDElementID: ");
		result.append(dvdElementID);
		result.append(", valueObject: ");
		result.append(valueObject);
		result.append(')');
		return result.toString();
	}

} //ExpectationImpl
