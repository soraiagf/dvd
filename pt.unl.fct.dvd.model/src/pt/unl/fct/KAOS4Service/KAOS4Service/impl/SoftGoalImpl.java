/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.Requirement;
import pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Soft Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl#getToRequirement <em>To Requirement</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.SoftGoalImpl#getDVDElementID <em>DVD Element ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftGoalImpl extends GoalImpl implements SoftGoal {
	/**
	 * The cached value of the '{@link #getToRequirement() <em>To Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToRequirement()
	 * @generated
	 * @ordered
	 */
	protected Requirement toRequirement;

	/**
	 * The default value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int DVD_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected int dvdElementID = DVD_ELEMENT_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.SOFT_GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement getToRequirement() {
		if (toRequirement != null && toRequirement.eIsProxy()) {
			InternalEObject oldToRequirement = (InternalEObject)toRequirement;
			toRequirement = (Requirement)eResolveProxy(oldToRequirement);
			if (toRequirement != oldToRequirement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT, oldToRequirement, toRequirement));
			}
		}
		return toRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement basicGetToRequirement() {
		return toRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToRequirement(Requirement newToRequirement, NotificationChain msgs) {
		Requirement oldToRequirement = toRequirement;
		toRequirement = newToRequirement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT, oldToRequirement, newToRequirement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToRequirement(Requirement newToRequirement) {
		if (newToRequirement != toRequirement) {
			NotificationChain msgs = null;
			if (toRequirement != null)
				msgs = ((InternalEObject)toRequirement).eInverseRemove(this, KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG, Requirement.class, msgs);
			if (newToRequirement != null)
				msgs = ((InternalEObject)newToRequirement).eInverseAdd(this, KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG, Requirement.class, msgs);
			msgs = basicSetToRequirement(newToRequirement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT, newToRequirement, newToRequirement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDVDElementID() {
		return dvdElementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDVDElementID(int newDVDElementID) {
		int oldDVDElementID = dvdElementID;
		dvdElementID = newDVDElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.SOFT_GOAL__DVD_ELEMENT_ID, oldDVDElementID, dvdElementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				if (toRequirement != null)
					msgs = ((InternalEObject)toRequirement).eInverseRemove(this, KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG, Requirement.class, msgs);
				return basicSetToRequirement((Requirement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				return basicSetToRequirement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				if (resolve) return getToRequirement();
				return basicGetToRequirement();
			case KAOS4ServicePackage.SOFT_GOAL__DVD_ELEMENT_ID:
				return getDVDElementID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				setToRequirement((Requirement)newValue);
				return;
			case KAOS4ServicePackage.SOFT_GOAL__DVD_ELEMENT_ID:
				setDVDElementID((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				setToRequirement((Requirement)null);
				return;
			case KAOS4ServicePackage.SOFT_GOAL__DVD_ELEMENT_ID:
				setDVDElementID(DVD_ELEMENT_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT:
				return toRequirement != null;
			case KAOS4ServicePackage.SOFT_GOAL__DVD_ELEMENT_ID:
				return dvdElementID != DVD_ELEMENT_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DVDElementID: ");
		result.append(dvdElementID);
		result.append(')');
		return result.toString();
	}

} //SoftGoalImpl
