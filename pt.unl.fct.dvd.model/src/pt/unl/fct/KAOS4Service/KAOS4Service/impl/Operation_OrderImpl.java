/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import pt.unl.fct.KAOS4Service.KAOS4Service.Agent;
import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.Operation;
import pt.unl.fct.KAOS4Service.KAOS4Service.Operation_Order;
import pt.unl.fct.KAOS4Service.KAOS4Service.Refinement;
import pt.unl.fct.KAOS4Service.KAOS4Service.Scenario;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Order</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl#getAgent <em>Agent</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl#getORefinement <em>ORefinement</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.Operation_OrderImpl#getScenario <em>Scenario</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Operation_OrderImpl extends NodeImpl implements Operation_Order {
	/**
	 * The default value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected String operation = OPERATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAgent() <em>Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgent()
	 * @generated
	 * @ordered
	 */
	protected Agent agent;

	/**
	 * The cached value of the '{@link #getORefinement() <em>ORefinement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getORefinement()
	 * @generated
	 * @ordered
	 */
	protected Refinement oRefinement;

	/**
	 * The default value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected static final int ORDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected int order = ORDER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getScenario() <em>Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenario()
	 * @generated
	 * @ordered
	 */
	protected Scenario scenario;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Operation_OrderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.OPERATION_ORDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(String newOperation) {
		String oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getAgent() {
		if (agent != null && agent.eIsProxy()) {
			InternalEObject oldAgent = (InternalEObject)agent;
			agent = (Agent)eResolveProxy(oldAgent);
			if (agent != oldAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.OPERATION_ORDER__AGENT, oldAgent, agent));
			}
		}
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent basicGetAgent() {
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAgent(Agent newAgent, NotificationChain msgs) {
		Agent oldAgent = agent;
		agent = newAgent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__AGENT, oldAgent, newAgent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgent(Agent newAgent) {
		if (newAgent != agent) {
			NotificationChain msgs = null;
			if (agent != null)
				msgs = ((InternalEObject)agent).eInverseRemove(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
			if (newAgent != null)
				msgs = ((InternalEObject)newAgent).eInverseAdd(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
			msgs = basicSetAgent(newAgent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__AGENT, newAgent, newAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement getORefinement() {
		if (oRefinement != null && oRefinement.eIsProxy()) {
			InternalEObject oldORefinement = (InternalEObject)oRefinement;
			oRefinement = (Refinement)eResolveProxy(oldORefinement);
			if (oRefinement != oldORefinement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT, oldORefinement, oRefinement));
			}
		}
		return oRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Refinement basicGetORefinement() {
		return oRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetORefinement(Refinement newORefinement, NotificationChain msgs) {
		Refinement oldORefinement = oRefinement;
		oRefinement = newORefinement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT, oldORefinement, newORefinement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setORefinement(Refinement newORefinement) {
		if (newORefinement != oRefinement) {
			NotificationChain msgs = null;
			if (oRefinement != null)
				msgs = ((InternalEObject)oRefinement).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
			if (newORefinement != null)
				msgs = ((InternalEObject)newORefinement).eInverseAdd(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
			msgs = basicSetORefinement(newORefinement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT, newORefinement, newORefinement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(int newOrder) {
		int oldOrder = order;
		order = newOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__ORDER, oldOrder, order));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario getScenario() {
		if (scenario != null && scenario.eIsProxy()) {
			InternalEObject oldScenario = (InternalEObject)scenario;
			scenario = (Scenario)eResolveProxy(oldScenario);
			if (scenario != oldScenario) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.OPERATION_ORDER__SCENARIO, oldScenario, scenario));
			}
		}
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario basicGetScenario() {
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScenario(Scenario newScenario, NotificationChain msgs) {
		Scenario oldScenario = scenario;
		scenario = newScenario;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__SCENARIO, oldScenario, newScenario);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenario(Scenario newScenario) {
		if (newScenario != scenario) {
			NotificationChain msgs = null;
			if (scenario != null)
				msgs = ((InternalEObject)scenario).eInverseRemove(this, KAOS4ServicePackage.SCENARIO__OPERATION, Scenario.class, msgs);
			if (newScenario != null)
				msgs = ((InternalEObject)newScenario).eInverseAdd(this, KAOS4ServicePackage.SCENARIO__OPERATION, Scenario.class, msgs);
			msgs = basicSetScenario(newScenario, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.OPERATION_ORDER__SCENARIO, newScenario, newScenario));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				if (agent != null)
					msgs = ((InternalEObject)agent).eInverseRemove(this, KAOS4ServicePackage.AGENT__OPERATION, Agent.class, msgs);
				return basicSetAgent((Agent)otherEnd, msgs);
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				if (oRefinement != null)
					msgs = ((InternalEObject)oRefinement).eInverseRemove(this, KAOS4ServicePackage.REFINEMENT__OPERATION, Refinement.class, msgs);
				return basicSetORefinement((Refinement)otherEnd, msgs);
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				if (scenario != null)
					msgs = ((InternalEObject)scenario).eInverseRemove(this, KAOS4ServicePackage.SCENARIO__OPERATION, Scenario.class, msgs);
				return basicSetScenario((Scenario)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				return basicSetAgent(null, msgs);
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				return basicSetORefinement(null, msgs);
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				return basicSetScenario(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__OPERATION:
				return getOperation();
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				if (resolve) return getAgent();
				return basicGetAgent();
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				if (resolve) return getORefinement();
				return basicGetORefinement();
			case KAOS4ServicePackage.OPERATION_ORDER__ORDER:
				return getOrder();
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				if (resolve) return getScenario();
				return basicGetScenario();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__OPERATION:
				setOperation((String)newValue);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				setAgent((Agent)newValue);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				setORefinement((Refinement)newValue);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__ORDER:
				setOrder((Integer)newValue);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				setScenario((Scenario)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__OPERATION:
				setOperation(OPERATION_EDEFAULT);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				setAgent((Agent)null);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				setORefinement((Refinement)null);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__ORDER:
				setOrder(ORDER_EDEFAULT);
				return;
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				setScenario((Scenario)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.OPERATION_ORDER__OPERATION:
				return OPERATION_EDEFAULT == null ? operation != null : !OPERATION_EDEFAULT.equals(operation);
			case KAOS4ServicePackage.OPERATION_ORDER__AGENT:
				return agent != null;
			case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT:
				return oRefinement != null;
			case KAOS4ServicePackage.OPERATION_ORDER__ORDER:
				return order != ORDER_EDEFAULT;
			case KAOS4ServicePackage.OPERATION_ORDER__SCENARIO:
				return scenario != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Operation.class) {
			switch (derivedFeatureID) {
				case KAOS4ServicePackage.OPERATION_ORDER__OPERATION: return KAOS4ServicePackage.OPERATION__OPERATION;
				case KAOS4ServicePackage.OPERATION_ORDER__AGENT: return KAOS4ServicePackage.OPERATION__AGENT;
				case KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT: return KAOS4ServicePackage.OPERATION__OREFINEMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Operation.class) {
			switch (baseFeatureID) {
				case KAOS4ServicePackage.OPERATION__OPERATION: return KAOS4ServicePackage.OPERATION_ORDER__OPERATION;
				case KAOS4ServicePackage.OPERATION__AGENT: return KAOS4ServicePackage.OPERATION_ORDER__AGENT;
				case KAOS4ServicePackage.OPERATION__OREFINEMENT: return KAOS4ServicePackage.OPERATION_ORDER__OREFINEMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operation: ");
		result.append(operation);
		result.append(", order: ");
		result.append(order);
		result.append(')');
		return result.toString();
	}

} //Operation_OrderImpl
