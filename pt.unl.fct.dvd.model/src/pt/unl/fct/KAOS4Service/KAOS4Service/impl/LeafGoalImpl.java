/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import org.eclipse.emf.ecore.EClass;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.LeafGoal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Leaf Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LeafGoalImpl extends GoalImpl implements LeafGoal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.LEAF_GOAL;
	}

} //LeafGoalImpl
