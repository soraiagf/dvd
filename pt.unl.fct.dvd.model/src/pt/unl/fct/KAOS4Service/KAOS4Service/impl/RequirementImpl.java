/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;
import pt.unl.fct.KAOS4Service.KAOS4Service.Requirement;
import pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal;
import pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl#getRequireSG <em>Require SG</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl#getValueObject <em>Value Object</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.impl.RequirementImpl#getSoftwareAgent <em>Software Agent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends LeafGoalImpl implements Requirement {
	/**
	 * The cached value of the '{@link #getRequireSG() <em>Require SG</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequireSG()
	 * @generated
	 * @ordered
	 */
	protected EList<SoftGoal> requireSG;

	/**
	 * The default value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int DVD_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDVDElementID() <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDVDElementID()
	 * @generated
	 * @ordered
	 */
	protected int dvdElementID = DVD_ELEMENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueObject() <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueObject()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_OBJECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueObject() <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueObject()
	 * @generated
	 * @ordered
	 */
	protected String valueObject = VALUE_OBJECT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSoftwareAgent() <em>Software Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftwareAgent()
	 * @generated
	 * @ordered
	 */
	protected SoftwareAgent softwareAgent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KAOS4ServicePackage.Literals.REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SoftGoal> getRequireSG() {
		if (requireSG == null) {
			requireSG = new EObjectWithInverseResolvingEList<SoftGoal>(SoftGoal.class, this, KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG, KAOS4ServicePackage.SOFT_GOAL__TO_REQUIREMENT);
		}
		return requireSG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDVDElementID() {
		return dvdElementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDVDElementID(int newDVDElementID) {
		int oldDVDElementID = dvdElementID;
		dvdElementID = newDVDElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REQUIREMENT__DVD_ELEMENT_ID, oldDVDElementID, dvdElementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueObject() {
		return valueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueObject(String newValueObject) {
		String oldValueObject = valueObject;
		valueObject = newValueObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REQUIREMENT__VALUE_OBJECT, oldValueObject, valueObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareAgent getSoftwareAgent() {
		if (softwareAgent != null && softwareAgent.eIsProxy()) {
			InternalEObject oldSoftwareAgent = (InternalEObject)softwareAgent;
			softwareAgent = (SoftwareAgent)eResolveProxy(oldSoftwareAgent);
			if (softwareAgent != oldSoftwareAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT, oldSoftwareAgent, softwareAgent));
			}
		}
		return softwareAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareAgent basicGetSoftwareAgent() {
		return softwareAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSoftwareAgent(SoftwareAgent newSoftwareAgent, NotificationChain msgs) {
		SoftwareAgent oldSoftwareAgent = softwareAgent;
		softwareAgent = newSoftwareAgent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT, oldSoftwareAgent, newSoftwareAgent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareAgent(SoftwareAgent newSoftwareAgent) {
		if (newSoftwareAgent != softwareAgent) {
			NotificationChain msgs = null;
			if (softwareAgent != null)
				msgs = ((InternalEObject)softwareAgent).eInverseRemove(this, KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY, SoftwareAgent.class, msgs);
			if (newSoftwareAgent != null)
				msgs = ((InternalEObject)newSoftwareAgent).eInverseAdd(this, KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY, SoftwareAgent.class, msgs);
			msgs = basicSetSoftwareAgent(newSoftwareAgent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT, newSoftwareAgent, newSoftwareAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRequireSG()).basicAdd(otherEnd, msgs);
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				if (softwareAgent != null)
					msgs = ((InternalEObject)softwareAgent).eInverseRemove(this, KAOS4ServicePackage.SOFTWARE_AGENT__RRESPONSABILITY, SoftwareAgent.class, msgs);
				return basicSetSoftwareAgent((SoftwareAgent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				return ((InternalEList<?>)getRequireSG()).basicRemove(otherEnd, msgs);
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				return basicSetSoftwareAgent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				return getRequireSG();
			case KAOS4ServicePackage.REQUIREMENT__DVD_ELEMENT_ID:
				return getDVDElementID();
			case KAOS4ServicePackage.REQUIREMENT__VALUE_OBJECT:
				return getValueObject();
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				if (resolve) return getSoftwareAgent();
				return basicGetSoftwareAgent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				getRequireSG().clear();
				getRequireSG().addAll((Collection<? extends SoftGoal>)newValue);
				return;
			case KAOS4ServicePackage.REQUIREMENT__DVD_ELEMENT_ID:
				setDVDElementID((Integer)newValue);
				return;
			case KAOS4ServicePackage.REQUIREMENT__VALUE_OBJECT:
				setValueObject((String)newValue);
				return;
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				setSoftwareAgent((SoftwareAgent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				getRequireSG().clear();
				return;
			case KAOS4ServicePackage.REQUIREMENT__DVD_ELEMENT_ID:
				setDVDElementID(DVD_ELEMENT_ID_EDEFAULT);
				return;
			case KAOS4ServicePackage.REQUIREMENT__VALUE_OBJECT:
				setValueObject(VALUE_OBJECT_EDEFAULT);
				return;
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				setSoftwareAgent((SoftwareAgent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KAOS4ServicePackage.REQUIREMENT__REQUIRE_SG:
				return requireSG != null && !requireSG.isEmpty();
			case KAOS4ServicePackage.REQUIREMENT__DVD_ELEMENT_ID:
				return dvdElementID != DVD_ELEMENT_ID_EDEFAULT;
			case KAOS4ServicePackage.REQUIREMENT__VALUE_OBJECT:
				return VALUE_OBJECT_EDEFAULT == null ? valueObject != null : !VALUE_OBJECT_EDEFAULT.equals(valueObject);
			case KAOS4ServicePackage.REQUIREMENT__SOFTWARE_AGENT:
				return softwareAgent != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DVDElementID: ");
		result.append(dvdElementID);
		result.append(", valueObject: ");
		result.append(valueObject);
		result.append(')');
		return result.toString();
	}

} //RequirementImpl
