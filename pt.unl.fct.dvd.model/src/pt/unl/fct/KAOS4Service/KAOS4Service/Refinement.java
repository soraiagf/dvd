/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogical <em>Logical</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasGoal <em>Has Goal</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getOperation <em>Operation</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical <em>Has Logical</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogicalO <em>Logical O</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement()
 * @model
 * @generated
 */
public interface Refinement extends Relation {
	/**
	 * Returns the value of the '<em><b>Logical</b></em>' attribute.
	 * The literals are from the enumeration {@link pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical</em>' attribute.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator
	 * @see #setLogical(LogicalOperator)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement_Logical()
	 * @model
	 * @generated
	 */
	LogicalOperator getLogical();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogical <em>Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical</em>' attribute.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.LogicalOperator
	 * @see #getLogical()
	 * @generated
	 */
	void setLogical(LogicalOperator value);

	/**
	 * Returns the value of the '<em><b>Has Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Goal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Goal</em>' reference.
	 * @see #setHasGoal(Goal)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement_HasGoal()
	 * @model
	 * @generated
	 */
	Goal getHasGoal();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasGoal <em>Has Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Goal</em>' reference.
	 * @see #getHasGoal()
	 * @generated
	 */
	void setHasGoal(Goal value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getORefinement <em>ORefinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement_Operation()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getORefinement
	 * @model opposite="ORefinement"
	 * @generated
	 */
	EList<Operation> getOperation();

	/**
	 * Returns the value of the '<em><b>Has Logical</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogicalO <em>Logical O</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Logical</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Logical</em>' reference.
	 * @see #setHasLogical(Refinement)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement_HasLogical()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getLogicalO
	 * @model opposite="logicalO"
	 * @generated
	 */
	Refinement getHasLogical();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical <em>Has Logical</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Logical</em>' reference.
	 * @see #getHasLogical()
	 * @generated
	 */
	void setHasLogical(Refinement value);

	/**
	 * Returns the value of the '<em><b>Logical O</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical <em>Has Logical</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical O</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical O</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRefinement_LogicalO()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Refinement#getHasLogical
	 * @model opposite="hasLogical"
	 * @generated
	 */
	EList<Refinement> getLogicalO();

} // Refinement
