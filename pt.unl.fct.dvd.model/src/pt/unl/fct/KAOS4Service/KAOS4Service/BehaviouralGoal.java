/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behavioural Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal#getDVDElementID <em>DVD Element ID</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getBehaviouralGoal()
 * @model
 * @generated
 */
public interface BehaviouralGoal extends Goal {
	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getBehaviouralGoal_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.BehaviouralGoal#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

} // BehaviouralGoal
