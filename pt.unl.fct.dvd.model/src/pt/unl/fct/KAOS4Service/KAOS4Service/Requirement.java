/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getRequireSG <em>Require SG</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getValueObject <em>Value Object</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent <em>Software Agent</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRequirement()
 * @model
 * @generated
 */
public interface Requirement extends LeafGoal {
	/**
	 * Returns the value of the '<em><b>Require SG</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement <em>To Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Require SG</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Require SG</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRequirement_RequireSG()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement
	 * @model opposite="toRequirement"
	 * @generated
	 */
	EList<SoftGoal> getRequireSG();

	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRequirement_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

	/**
	 * Returns the value of the '<em><b>Value Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Object</em>' attribute.
	 * @see #setValueObject(String)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRequirement_ValueObject()
	 * @model
	 * @generated
	 */
	String getValueObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getValueObject <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Object</em>' attribute.
	 * @see #getValueObject()
	 * @generated
	 */
	void setValueObject(String value);

	/**
	 * Returns the value of the '<em><b>Software Agent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getRResponsability <em>RResponsability</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Agent</em>' reference.
	 * @see #setSoftwareAgent(SoftwareAgent)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getRequirement_SoftwareAgent()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getRResponsability
	 * @model opposite="RResponsability" required="true"
	 * @generated
	 */
	SoftwareAgent getSoftwareAgent();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent <em>Software Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Agent</em>' reference.
	 * @see #getSoftwareAgent()
	 * @generated
	 */
	void setSoftwareAgent(SoftwareAgent value);

} // Requirement
