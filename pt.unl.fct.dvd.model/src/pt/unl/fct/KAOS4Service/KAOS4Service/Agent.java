/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getName <em>Name</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getAgent()
 * @model abstract="true"
 * @generated
 */
public interface Agent extends Node {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getAgent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Agent#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getAgent_Operation()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Operation#getAgent
	 * @model opposite="Agent"
	 * @generated
	 */
	EList<Operation> getOperation();

} // Agent
