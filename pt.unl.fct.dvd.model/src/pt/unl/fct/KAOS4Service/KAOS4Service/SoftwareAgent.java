/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getRResponsability <em>RResponsability</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftwareAgent()
 * @model
 * @generated
 */
public interface SoftwareAgent extends Agent {
	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specialization</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftwareAgent_Specialization()
	 * @model
	 * @generated
	 */
	EList<SoftwareAgent> getSpecialization();

	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftwareAgent_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftwareAgent#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

	/**
	 * Returns the value of the '<em><b>RResponsability</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent <em>Software Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RResponsability</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RResponsability</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftwareAgent_RResponsability()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getSoftwareAgent
	 * @model opposite="SoftwareAgent"
	 * @generated
	 */
	EList<Requirement> getRResponsability();

} // SoftwareAgent
