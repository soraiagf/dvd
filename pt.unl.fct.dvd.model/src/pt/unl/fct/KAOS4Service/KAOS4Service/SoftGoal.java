/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Soft Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement <em>To Requirement</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getDVDElementID <em>DVD Element ID</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftGoal()
 * @model
 * @generated
 */
public interface SoftGoal extends Goal {
	/**
	 * Returns the value of the '<em><b>To Requirement</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getRequireSG <em>Require SG</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Requirement</em>' reference.
	 * @see #setToRequirement(Requirement)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftGoal_ToRequirement()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Requirement#getRequireSG
	 * @model opposite="requireSG"
	 * @generated
	 */
	Requirement getToRequirement();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getToRequirement <em>To Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Requirement</em>' reference.
	 * @see #getToRequirement()
	 * @generated
	 */
	void setToRequirement(Requirement value);

	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getSoftGoal_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.SoftGoal#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

} // SoftGoal
