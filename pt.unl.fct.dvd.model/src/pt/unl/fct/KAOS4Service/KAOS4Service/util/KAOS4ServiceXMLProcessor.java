/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class KAOS4ServiceXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KAOS4ServiceXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		KAOS4ServicePackage.eINSTANCE.eClass();
	}
	
	/**
	 * Register for "*" and "xml" file extensions the KAOS4ServiceResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new KAOS4ServiceResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new KAOS4ServiceResourceFactoryImpl());
		}
		return registrations;
	}

} //KAOS4ServiceXMLProcessor
