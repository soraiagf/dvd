/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.util.KAOS4ServiceResourceFactoryImpl
 * @generated
 */
public class KAOS4ServiceResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public KAOS4ServiceResourceImpl(URI uri) {
		super(uri);
	}

} //KAOS4ServiceResourceImpl
