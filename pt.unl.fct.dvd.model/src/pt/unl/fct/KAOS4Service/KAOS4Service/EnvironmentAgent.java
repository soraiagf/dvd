/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getEResponsability <em>EResponsability</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getDVDElementID <em>DVD Element ID</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getEnvironmentAgent()
 * @model
 * @generated
 */
public interface EnvironmentAgent extends Agent {
	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specialization</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getEnvironmentAgent_Specialization()
	 * @model
	 * @generated
	 */
	EList<EnvironmentAgent> getSpecialization();

	/**
	 * Returns the value of the '<em><b>EResponsability</b></em>' reference list.
	 * The list contents are of type {@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation}.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent <em>Environment Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EResponsability</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EResponsability</em>' reference list.
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getEnvironmentAgent_EResponsability()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent
	 * @model opposite="EnvironmentAgent"
	 * @generated
	 */
	EList<Expectation> getEResponsability();

	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getEnvironmentAgent_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

} // EnvironmentAgent
