/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expectation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent <em>Environment Agent</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getDVDElementID <em>DVD Element ID</em>}</li>
 *   <li>{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getValueObject <em>Value Object</em>}</li>
 * </ul>
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getExpectation()
 * @model
 * @generated
 */
public interface Expectation extends LeafGoal {
	/**
	 * Returns the value of the '<em><b>Environment Agent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getEResponsability <em>EResponsability</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Agent</em>' reference.
	 * @see #setEnvironmentAgent(EnvironmentAgent)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getExpectation_EnvironmentAgent()
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.EnvironmentAgent#getEResponsability
	 * @model opposite="EResponsability" required="true"
	 * @generated
	 */
	EnvironmentAgent getEnvironmentAgent();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getEnvironmentAgent <em>Environment Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment Agent</em>' reference.
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	void setEnvironmentAgent(EnvironmentAgent value);

	/**
	 * Returns the value of the '<em><b>DVD Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DVD Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DVD Element ID</em>' attribute.
	 * @see #setDVDElementID(int)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getExpectation_DVDElementID()
	 * @model required="true"
	 * @generated
	 */
	int getDVDElementID();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getDVDElementID <em>DVD Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DVD Element ID</em>' attribute.
	 * @see #getDVDElementID()
	 * @generated
	 */
	void setDVDElementID(int value);

	/**
	 * Returns the value of the '<em><b>Value Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Object</em>' attribute.
	 * @see #setValueObject(String)
	 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getExpectation_ValueObject()
	 * @model
	 * @generated
	 */
	String getValueObject();

	/**
	 * Sets the value of the '{@link pt.unl.fct.KAOS4Service.KAOS4Service.Expectation#getValueObject <em>Value Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Object</em>' attribute.
	 * @see #getValueObject()
	 * @generated
	 */
	void setValueObject(String value);

} // Expectation
