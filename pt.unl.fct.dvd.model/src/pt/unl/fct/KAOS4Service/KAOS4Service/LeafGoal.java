/**
 */
package pt.unl.fct.KAOS4Service.KAOS4Service;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.unl.fct.KAOS4Service.KAOS4Service.KAOS4ServicePackage#getLeafGoal()
 * @model abstract="true"
 * @generated
 */
public interface LeafGoal extends Goal {
} // LeafGoal
